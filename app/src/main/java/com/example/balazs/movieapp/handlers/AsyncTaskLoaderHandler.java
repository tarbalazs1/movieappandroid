package com.example.balazs.movieapp.handlers;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.fragments.FragmentMovieChooser;
import com.example.balazs.movieapp.fragments.MovieTitleDialog;

import java.util.ArrayList;

/**
 * This is a handler class that helps show the dialogFragment with the downloaded titles
 * Created by Balázs on 16/03/17.
 */
public class AsyncTaskLoaderHandler extends Handler {

    private Fragment mMovireChooserFragment;
    private ArrayList<String> mMovieList;

    public AsyncTaskLoaderHandler(Fragment mISelectedMovie, ArrayList<String> mMovieList) {
        this.mMovireChooserFragment = mISelectedMovie;
        this.mMovieList = mMovieList;
    }

    /**This method receive the message, and start the dialog with the title list
     * @param msg Message
     */
    @Override
    public void handleMessage(Message msg) {
        if (msg.what == 2) {
            FragmentManager fgm = mMovireChooserFragment.getChildFragmentManager();
            MovieTitleDialog movieTitleDialog = new MovieTitleDialog();
            movieTitleDialog.setParentFragment((FragmentMovieChooser) mMovireChooserFragment);
            movieTitleDialog.setmMovieTitleList(mMovieList);
            movieTitleDialog.show(fgm, mMovireChooserFragment.getString(R.string.movie_title_dialog_tag));
        }
    }


}
