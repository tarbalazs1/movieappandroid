package com.example.balazs.movieapp.interfaces;

/**Communication between SimpleTouchCallback and FragmentMovieChooser.
 * Created by Balázs on 16/03/04.
 */
public interface SwipeListener {

    void onSwipe(int position);
}
