package com.example.balazs.movieapp.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.balazs.movieapp.classes.AsyncMovie;
import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.activities.ActivitySeasonInfo;
import com.example.balazs.movieapp.beans.Movie;
import com.example.balazs.movieapp.beans.Season;
import com.example.balazs.movieapp.classes.SaveImage;
import com.example.balazs.movieapp.extras.Util;

import java.util.ArrayList;

/**This is a fragment class that shows the details of the current movie or season
 *
 * Created by Balázs on 16/03/08.
 */
public class FragmentMovieInfo extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Object>,
        OnLongClickListener {

    private final String TAG = FragmentMovieInfo.class.getCanonicalName();

    private static final int SEASON_INFO_TASK_ID = 4;
    private ImageView mPoster;
    private TextView mTitle;
    private TextView mRuntime;
    private TextView mReleased;
    private TextView mCountry;
    private TextView mImdbRating;
    private TextView mLanguage;
    private TextView mPlot;

    private boolean mIsVisible;
    private ImageView mFullscreenPoster;
    private int mSeason;

    private int mAsyncID;
    private Movie mMovie;

    private ImageButton mIbSeasonLeft;
    private ImageButton mIbSeasonRight;
    private Button mBtnSeason;
    private String mURL;
    private boolean mAsyncBoolean;
    private ScrollView mScrollView;


    /**
     * Lifecycle method creates and returns the view hierarchy associated with the fragment
     *
     * @param inflater           LayoutInflater object that can be used to inflate any views
     * @param container          The parent view
     * @param savedInstanceState bundle that used for return the saved data
     * @return Return the View for the fragment's UI
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_info, container, false);

        mRuntime = (TextView) rootView.findViewById(R.id.tv_runtime);
        mReleased = (TextView) rootView.findViewById(R.id.tv_released);
        mImdbRating = (TextView) rootView.findViewById(R.id.tv_imdbrating);
        mLanguage = (TextView) rootView.findViewById(R.id.tv_language);
        mPlot = (TextView) rootView.findViewById(R.id.tv_plot);
        mCountry = (TextView) rootView.findViewById(R.id.tv_country);

        mTitle = (TextView) rootView.findViewById(R.id.tv_movie_title);

        mPoster = (ImageView) rootView.findViewById(R.id.iv_poster);

        mFullscreenPoster = (ImageView) rootView.findViewById(R.id.iv_fullscreen_poster);
        mIbSeasonLeft = (ImageButton) rootView.findViewById(R.id.ib_season_left);
        mIbSeasonRight = (ImageButton) rootView.findViewById(R.id.ib_season_right);
        mBtnSeason = (Button) rootView.findViewById(R.id.btn_season);
        mScrollView = (ScrollView) rootView.findViewById(R.id.sv_plot);


        ImageButton mFacebookShare = (ImageButton) rootView.findViewById(R.id.ib_facebook);
        ImageButton mIMDbLink = (ImageButton) rootView.findViewById(R.id.ib_imdb);

        mFacebookShare.setOnClickListener(this);
        mIMDbLink.setOnClickListener(this);
        mPoster.setOnClickListener(this);
        mFullscreenPoster.setOnLongClickListener(this);
        mFullscreenPoster.setOnClickListener(this);


        mSeason = 1;

        if (savedInstanceState != null) {
            mAsyncBoolean = savedInstanceState.getBoolean("Async");
            mSeason = savedInstanceState.getInt("Season");
            mAsyncID = savedInstanceState.getInt("AsyncID");
            mIsVisible = savedInstanceState.getBoolean("Visible");
            Log.i(TAG, "savedinstance " + mAsyncID);
        }

        if (mAsyncID != 0) {
            Log.i(TAG, "if else " + mAsyncID);

            getActivity().getSupportLoaderManager().initLoader(mAsyncID, null, this).forceLoad();
        }
        if (mIsVisible) {
            showFullScreenPoster();
        }
        return rootView;
    }

    /**
     * In this lifecycle method, when the activity was created I ask for the mMovie object
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //mMovie = ((ActivityMovieInfo) getActivity()).getmMovie();
        mMovie = getActivity().getIntent().getParcelableExtra("MOVIE");
        enableButtons(mMovie.getType().equals("series"));
        setMovieData();
    }

    /**
     * If the type is "series" then set onnclicklistener for the correct buttons. If the type is
     * not "series" than no need buttons, and onclicklistener so they will not be visible. In this case
     * the height of the scrollview will be increasing.
     *
     * @param isSeries series or not
     */
    private void enableButtons(boolean isSeries) {
        if (isSeries) {
            mIbSeasonRight.setOnClickListener(this);
            mIbSeasonLeft.setOnClickListener(this);
            mBtnSeason.setOnClickListener(this);
            mBtnSeason.setText("S" + mSeason);

        } else {
            mBtnSeason.setVisibility(View.GONE);
            mIbSeasonRight.setVisibility(View.GONE);
            mIbSeasonLeft.setVisibility(View.GONE);


            PercentRelativeLayout.LayoutParams params = (PercentRelativeLayout.LayoutParams) mScrollView.getLayoutParams();
            // This will currently return null, if it was not constructed from XML.
            PercentLayoutHelper.PercentLayoutInfo info = params.getPercentLayoutInfo();
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                info.heightPercent = 0.38f;
            } else {
                info.heightPercent = 1.00f;
            }
            mScrollView.requestLayout();

        }
    }

    /**
     * Set the data of the movie or series with some html tags for example font color, italic, bold
     * for the better UI/UX
     */
    private void setMovieData() {

        mTitle.setText(Html.fromHtml(getResources().getString(R.string.html_title_start_tag) +
                mMovie.getTitle() + getResources().getString(R.string.html_title_end_tag)));

        mRuntime.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_runtime) + getResources().getString(R.string.html_details_end_tag) +
                mMovie.getRuntime()));

        mReleased.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_released) + getResources().getString(R.string.html_details_end_tag) +
                mMovie.getReleased()));

        mImdbRating.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_imdb_rating) + getResources().getString(R.string.html_details_end_tag) +
                mMovie.getImdbRating()));

        mLanguage.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_language) + getResources().getString(R.string.html_details_end_tag) +
                mMovie.getLanguage()));

        mPlot.setText(mMovie.getPlot());

        mCountry.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_country) + getResources().getString(R.string.html_details_end_tag) +
                mMovie.getCountry()));

        mPoster.setImageBitmap(Movie.getPoster());
        mFullscreenPoster.setImageBitmap(Movie.getPoster());
    }


    /**This method called when a view (facebook or imdb button, right-left arrows, season button)
     *  has been clicked
     * @param v  The view that was clicked
     */
    @Override
    public void onClick(View v) {
        if (Util.isNetworkAvailable(getContext())) {
            switch (v.getId()) {
                case R.id.ib_facebook: {
                    Util.startFacebookShareDialog(mMovie.getImdbID(), getActivity());
                    Util.facebookNotification(getContext());
                    break;
                }
                case R.id.ib_imdb: {
                    Util.startImdbWebPage(mMovie.getImdbID(), getContext());
                    break;
                }
                case R.id.ib_season_left:
                    if (mSeason > 1) {
                        mSeason--;
                    } else {
                        //mSeason = getResources().getInteger(R.integer.default_season);
                        mSeason = Integer.parseInt(mMovie.getTotalSeasons());
                    }
                    mBtnSeason.setText(String.format("%s%s", getResources().getString(R.string.str_season_letter), String.valueOf(mSeason)));
                    break;
                case R.id.ib_season_right:
                    if (mSeason < Integer.parseInt(mMovie.getTotalSeasons())) {
                        mSeason++;
                    } else {
                        mSeason = 1;
                    }
                    mBtnSeason.setText(String.format("%s%s", getResources().getString(R.string.str_season_letter), String.valueOf(mSeason)));
                    break;
                case R.id.btn_season:
                    mURL = Util.urlBuilder(mMovie.getTitle(), Util.SEASON_DETAILS_TAG, 0, 0, null, String.valueOf(mSeason), null);
                    Log.i(TAG, mURL);

                    mAsyncID = SEASON_INFO_TASK_ID;
                    Log.i(TAG, "initloader " + mAsyncID); //itt a helye
                    if (!mAsyncBoolean) {
                        mAsyncBoolean = true;
                        //ide tettem az inicializáláshoz szükséges változót ezért csak egyszer volt jó..
                        getActivity().getSupportLoaderManager().initLoader(SEASON_INFO_TASK_ID, null, this).forceLoad();

                    } else {
                        getActivity().getSupportLoaderManager().restartLoader(SEASON_INFO_TASK_ID, null, this).forceLoad();
                    }
                    break;
                case R.id.iv_poster: {
                    showFullScreenPoster();
                }
            }
        } else {
            Toast.makeText(getContext(), R.string.str_toast_no_internet, Toast.LENGTH_SHORT).show();
        }

        if (v.getId() == R.id.iv_fullscreen_poster) {
            mFullscreenPoster.setVisibility(View.GONE);
            mIsVisible = false;
        }

    }

    /**
     * This method shows you the poster that you can move, or resize if you want to
     */
    private void showFullScreenPoster() {
        if (mFullscreenPoster.getVisibility() == View.GONE) {
            mFullscreenPoster.setVisibility(View.VISIBLE);
            mIsVisible = true;

        }
    }

    /**Instantiate and return a new Loader for the given ID.
     * @param id The ID whose loader is to be created
     * @param args Any arguments supplied by the caller
     * @return
     */
    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new AsyncMovie(getActivity(), mURL);
    }

    /**Called when a previously created loader has finished its load.
     * @param loader The Loader that has finished.
     * @param data The data generated by the Loader. It is an ArrayList of season data
     */
    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        if (data != null && loader.getId() == SEASON_INFO_TASK_ID && data instanceof ArrayList<?>) {
            ArrayList<Season> seasonList = new ArrayList<>();
            for (Object o : (ArrayList<?>) data) {
                if (o instanceof Season) {
                    seasonList.add((Season) o);
                }
            }
            startSeasonInfoPage(seasonList);

        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.str_no_season_found_start) +
                    mSeason + getResources().getString(R.string.str_no_season_found_end), Toast.LENGTH_SHORT).show();
        }
        mAsyncID = 0;
        Log.i(TAG, "onloadfinish " + mAsyncID);

    }

    /**
     * Start the new activity with the arrayList which contains the data of the season
     *
     * @param seasonList arraylist that contains the data
     */
    private void startSeasonInfoPage(ArrayList<Season> seasonList) {
        Intent i = new Intent(getActivity(), ActivitySeasonInfo.class);
        i.putParcelableArrayListExtra("SEASON", seasonList);
        startActivity(i);
    }

    /**Called when a previously created loader is being reset, and thus making its data unavailable.
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    /**
     * Called to retrieve per-instance state from an activity before being killed so that the
     * state can be restored in onCreate(Bundle) or onRestoreInstanceState(Bundle)
     * @param outState Bundle in which to place my saved state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("Async", mAsyncBoolean);
        outState.putInt("Season", mSeason);
        outState.putInt("AsyncID", mAsyncID);
        outState.putBoolean("Visible", mIsVisible);
        super.onSaveInstanceState(outState);
    }

    /**Called when the ImageView has been clicked and held.
     * @param v The view that was clicked and held.
     * @return true if the callback consumed the long click, false otherwise.
     */
    @Override
    public boolean onLongClick(View v) {
        permissionForStorage();

        return true;
    }

    private void permissionForStorage() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        }else{
            if (mFullscreenPoster.getVisibility() == View.VISIBLE && Util.isExternalStorageIsWriteable()) {
                SaveImage saveImage = new SaveImage();
                saveImage.saveImage(getContext(), Movie.getPoster(), mMovie.getTitle().replaceAll(" ", ""));
            }
        }
    }
}
