package com.example.balazs.movieapp.interfaces;

/**Communication between MainFragment and MainActivity
 * Created by Balázs on 16/02/26.
 */
public interface ICommunicator {

    void onModeSelected(String tag);
}
