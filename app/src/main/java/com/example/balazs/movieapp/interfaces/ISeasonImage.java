package com.example.balazs.movieapp.interfaces;

/**Communication between SeasonListAdapter and FragmentSeasonInfo.
 * It lets the fragment know about the imdb icon click
 * Created by Balázs on 16/04/05.
 */
public interface ISeasonImage {

     void onEpisodeImdbClick(int position);
}
