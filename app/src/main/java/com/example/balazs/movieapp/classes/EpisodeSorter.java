package com.example.balazs.movieapp.classes;


import com.example.balazs.movieapp.beans.Season;

import java.util.Comparator;

/**
 * This class is used for sort the episodes by title, rating, and date
 *
 * Created by Balázs on 2015.11.07..
 */
public class EpisodeSorter implements Comparator<Season> {


    public final static boolean INC = true;
    public static final boolean DEC = false;

    private static ViewPoint chosenViewPoint;
    private static boolean how;

    public EpisodeSorter() {
    }

    /**
     * This method set an object against another, and make an order based on the chosen viewpoint,
     * increase, decrease
     * @param lhs first object
     * @param rhs second object
     * @return sorted episodes
     */
    @Override
    public int compare(Season lhs, Season rhs) {

        switch (chosenViewPoint) {

            case TITLE: {
                return (how) ? lhs.getEpisodeTitle().compareTo(rhs.getEpisodeTitle()) :
                        rhs.getEpisodeTitle().compareTo(lhs.getEpisodeTitle());
            }
            case RATING: {
                //"N/A" elszáll
                return (int) ((how) ? Math.signum(Float.valueOf(rhs.getEpisodeImdbRating()) -
                        Float.valueOf(lhs.getEpisodeImdbRating())) :
                        Math.signum(Float.valueOf(lhs.getEpisodeImdbRating()) -
                                Float.valueOf(rhs.getEpisodeImdbRating())));
            }
            case DATE: {
                return (how) ? Integer.valueOf(lhs.getEpisode()) - Integer.valueOf(rhs.getEpisode()) :
                        Integer.valueOf(rhs.getEpisode()) - Integer.valueOf(lhs.getEpisode());
            }
        }
        return 0;
    }

    public static ViewPoint getChosenViewPoint() {
        return chosenViewPoint;
    }

    public static boolean isHow() {
        return how;
    }

    public static void setChosenViewPoint(ViewPoint chosenViewPoint, boolean how) {
        EpisodeSorter.chosenViewPoint = chosenViewPoint;
        EpisodeSorter.how = how;
    }
}
