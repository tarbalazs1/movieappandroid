package com.example.balazs.movieapp.beans;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * This class is a simple bean for the Realm database
 *
 * Created by Balázs on 16/03/07.
 */
public class Title extends RealmObject {

    private String movieTitle;
    @PrimaryKey
    private long added;

    public Title() {
    }

    public Title(String movieTitle, long added) {
        this.movieTitle = movieTitle;
        this.added = added;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public long getAdded() {
        return added;
    }

    public void setAdded(long added) {
        this.added = added;
    }
}
