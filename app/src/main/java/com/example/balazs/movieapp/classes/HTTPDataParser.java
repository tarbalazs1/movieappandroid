package com.example.balazs.movieapp.classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.beans.Episode;
import com.example.balazs.movieapp.beans.Movie;
import com.example.balazs.movieapp.beans.Season;
import com.example.balazs.movieapp.extras.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This is the class that used for making objects from json data
 * <p>
 * Created by Balázs on 16/03/17.
 */
public class HTTPDataParser {


    private static final String TAG = HTTPDataParser.class.getSimpleName();
    private Context mContext;

    public HTTPDataParser(Context context) {
        this.mContext = context;
    }

    public HTTPDataParser() {

    }


    /**
     * This is the method that used for separate the different movie and episode details from the json
     *
     * @param movieJsonStr movie or episode json string
     * @param tag          String tag for distinction
     * @return return the movie, episode, or null object
     * @throws JSONException
     * @throws IOException
     */
    public Object getMovieDataFromJson(String movieJsonStr, String tag) throws JSONException, IOException {

        Movie movie;
        Episode episode;

        JSONObject movieJson = new JSONObject(movieJsonStr);
        boolean response = movieJson.getBoolean(Util.KEY_RESPONSE);

        if (response) {

            String title = movieJson.getString(Util.KEY_TITLE);
            String released = movieJson.getString(Util.KEY_RELEASED);
            String runtime = movieJson.getString(Util.KEY_RUNTIME);

            String plot = movieJson.getString(Util.KEY_PLOT);
            String language = movieJson.getString(Util.KEY_LANGUAGE);
            String country = movieJson.getString(Util.KEY_COUNTRY);
            String posterUrl = movieJson.getString(Util.KEY_POSTER);
            String imdbRating = movieJson.getString(Util.KEY_IMDBRATING);
            String type = movieJson.getString(Util.KEY_TYPE);
            String imdbID = movieJson.getString(Util.KEY_IMDBID);
            String totalSeasons;

            switch (tag) {
                case Util.MOVIE_DETAILS_TAG: {
                    if (type.equals("series")) {
                        totalSeasons = movieJson.getString(Util.KEY_TOTALSEASON);
                    } else {
                        totalSeasons = null;
                    }
                    movie = new Movie(title, released, runtime, plot, language, country, posterUrl,
                            imdbRating, type, imdbID, totalSeasons);
                    Movie.setPoster(downloadAndSetPoster(movie.getPosterUrl()));
                    return movie;
                }
                case Util.EPISODE_DETAILS_TAG: {
                    String season = movieJson.getString(Util.KEY_SEASON);
                    String episodeString = movieJson.getString(Util.KEY_EPISODE);
                    episode = new Episode(title, released, country, imdbRating, language, runtime,
                            plot, season, episodeString, posterUrl, imdbID);
                    Episode.setPoster(downloadAndSetPoster(episode.getPosterUrl()));
                    return episode;
                }
            }

        }
        return null;

    }

    /**
     * This is the method that is used for downloading the poster of the movie or episode
     *
     * @param posterUrl url for the poster
     * @return return the downloaded image or the default image if the movie or episode does not have poster image
     * @throws IOException
     */
    private Bitmap downloadAndSetPoster(String posterUrl) throws IOException {
        Bitmap poster;

        if (!posterUrl.equals("N/A")) {
            InputStream input = new URL(posterUrl).openStream();
            poster = BitmapFactory.decodeStream(input);
            return poster;
            //Movie.setPoster(poster);
        } else {
            poster = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.defaultpic);
            //Movie.setPoster(poster);
            return poster;
        }

    }

    /**
     * This method is used for separate the different movie or season list details from the json
     *
     * @param jsonString movie or season list string
     * @param tag        String tag for distinction
     * @return return the list of the movies, or episodes
     * @throws JSONException
     */
    public Object getListDataFromJson(String jsonString, String tag) throws JSONException {
        List<String> movieList;
        List<Season> seasonList;

        String title;
        JSONObject jsonObject;
        JSONArray jsonArray;
        boolean response;
        JSONObject jsonList = new JSONObject(jsonString);
        try {
            response = jsonList.getBoolean(Util.KEY_RESPONSE);
        } catch (Exception e) {
            Log.e(TAG, "error: " + e);
            return null;
            //talán ez kellett ahhoz hogy ne szálljon el? mármint a timeoutnál hm.
        }

        if (response) {
            switch (tag) {
                case Util.CHOSEN_MOVIE_TAG: {
                    jsonArray = jsonList.getJSONArray("Search");
                    movieList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        title = jsonObject.getString("Title");
                        movieList.add(title);
                    }
                    return movieList;
                }
                case Util.SEASON_DETAILS_TAG: {
                    Season.setSeason(jsonList.getString("Season"));
                    Season.setTitle(jsonList.getString("Title"));
                    jsonArray = jsonList.getJSONArray("Episodes");
                    String released, episode, imdbRating, imdbID;
                    seasonList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        title = jsonObject.getString("Title");
                        released = jsonObject.getString("Released");
                        episode = jsonObject.getString("Episode");
                        imdbRating = jsonObject.getString("imdbRating");
                        if (imdbRating.equals("N/A")) {
                            imdbRating = "0";
                        }

                        imdbID = jsonObject.getString("imdbID");

                        seasonList.add(new Season(title, released, episode, imdbRating, imdbID));
                    }
                    return seasonList;
                }
            }

        }
        return null;
    }

    /**
     * This method is used for separate the different place details from the json
     *
     * @param nearbyPlacesJsonStr nearby places json string
     * @return return the list of hashmaps that contains the data
     * @throws JSONException
     */
    public Object getPlacesFromJson(String nearbyPlacesJsonStr) throws JSONException {
        List<HashMap<String, String>> placesList = new ArrayList<>();
        HashMap<String, String> placeMap;

        JSONObject placesObject = new JSONObject(nearbyPlacesJsonStr);

        String response = placesObject.getString(Util.KEY_MAPS_STATUS);
        if (response.equals("OK")) {
            JSONArray placesArray = placesObject.getJSONArray("results");

            for (int i = 0; i < placesArray.length(); i++) {
                placeMap = getPlace((JSONObject) placesArray.get(i));
                placesList.add(placeMap);
            }
            return placesList;
        }
        return null;
    }

    /**
     * This method is used for separate the details of each places
     *
     * @param googlePlaceJson json string for each places
     * @return return a hashmap with the details of the current place
     * @throws JSONException
     */
    private HashMap<String, String> getPlace(JSONObject googlePlaceJson) throws JSONException {
        HashMap<String, String> googlePlaceMap = new HashMap<>();
        String placeName = "-NA-";
        String vicinity = "-NA-";
        String latitude;
        String longitude;
        String reference;
        if (!googlePlaceJson.isNull("name")) {
            placeName = googlePlaceJson.getString("name");
        }
        if (!googlePlaceJson.isNull("vicinity")) {
            vicinity = googlePlaceJson.getString("vicinity");
        }
        latitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lat");
        longitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lng");
        reference = googlePlaceJson.getString("reference");
        googlePlaceMap.put("place_name", placeName);
        googlePlaceMap.put("vicinity", vicinity);
        googlePlaceMap.put("lat", latitude);
        googlePlaceMap.put("lng", longitude);
        googlePlaceMap.put("reference", reference);

        return googlePlaceMap;
    }

}
