package com.example.balazs.movieapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.balazs.movieapp.interfaces.SwipeListener;

/**
 * Created by Balázs on 16/03/04.
 */
public class SimpleTouchCallback extends ItemTouchHelper.Callback {

    private SwipeListener mListener;

    public SimpleTouchCallback(SwipeListener swipeListener) {
        mListener = swipeListener;
    }

    /**Return a composite flag which defines the enabled move directions in each state (idle, swiping, dragging)
     * @param recyclerView The RecyclerView to which ItemTouchHelper is attached
     * @param viewHolder The ViewHolder for which the movement information is necessary
     * @return flag that specifying the allowed movements
     */
    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(0, ItemTouchHelper.END);
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return false;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    /**Called when a ViewHolder is swiped by the user.
     * @param viewHolder  The ViewHolder which has been swiped by the user
     * @param direction The direction to which the ViewHolder is swiped
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mListener.onSwipe(viewHolder.getAdapterPosition());
    }
}
