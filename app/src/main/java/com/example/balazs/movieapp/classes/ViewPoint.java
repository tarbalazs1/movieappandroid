package com.example.balazs.movieapp.classes;

/**
 * Viewpoints for the order
 * Created by Balázs on 2015.11.07..
 */
public enum ViewPoint {

    TITLE,RATING,DATE
}
