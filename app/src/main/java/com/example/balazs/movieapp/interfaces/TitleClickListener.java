package com.example.balazs.movieapp.interfaces;

/**Communication between AdapterTitle.TitleHolder and FragmentMovieChooser.
 * Created by Balázs on 16/03/04.
 */
public interface TitleClickListener {
    void onTitleClick(int position);
}
