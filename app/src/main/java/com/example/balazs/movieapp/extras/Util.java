package com.example.balazs.movieapp.extras;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.activities.MainActivity;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * This is a global class with some methods that are used by different classes, and also contains
 * the static final tags, constants, links
 * <p/>
 * Created by Balázs on 16/03/04.
 */
public class Util {

    private static final String TAG = Util.class.getSimpleName();

    public static void showViews(List<View> views) {
        for (View view : views) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void hideViews(List<View> views) {
        for (View view : views) {
            view.setVisibility(View.GONE);
        }
    }

    /**
     * This method checks if the device connected to the internet over wifi or mobile provider
     *
     * @param context context for SystemService
     * @return true or false
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true;
            }
        } else {
            // not connected to the internet
            return false;
        }
        return false;
    }

    /**
     * This method makes urls according to the tag variables
     *
     * @param title     title for movie, series, episode url
     * @param tag       tag for switch/case
     * @param latitude  latitude for nearby places url
     * @param longitude longitude for nearby places url
     * @param placeType place type for nearby places url
     * @param season    season for series url
     * @param episode   episode for episode url
     * @return string url
     */
    public static String urlBuilder(String title, String tag, double latitude,
                                    double longitude, String placeType, String season, String episode) {
        switch (tag) {
            case MOVIE_DETAILS_TAG: {
                Uri builtUri = Uri.parse(OMDB_BASE_URL).buildUpon().
                        appendQueryParameter(TITLE_PARAM, title)
                        .appendQueryParameter(PLOT_PARAM, plot)
                        .appendQueryParameter(TYPE_PARAM, type).build();

                return builtUri.toString();
            }
            case CHOSEN_MOVIE_TAG: {
                Uri builtUri = Uri.parse(OMDB_BASE_URL).buildUpon().
                        appendQueryParameter(CHOSEN_TITLE_PARAM, title)
                        .appendQueryParameter(PAGE_PARAM, String.valueOf(page))
                        .appendQueryParameter(TYPE_PARAM, type).build();

                return builtUri.toString();
            }
            case NEARBY_PLACES_TAG: {
                Uri builtUri = Uri.parse(NEARBY_BASE_URL).buildUpon().
                        appendQueryParameter(LOCATION_PARAM, String.valueOf(latitude + "," + longitude))
                        .appendQueryParameter(RADIUS_PARAM, String.valueOf(PROXIMITY_RADIUS))
                        .appendQueryParameter(MAP_TYPE_PARAM, placeType)
                        .appendQueryParameter(SENSOR_PARAM, sensor)
                        .appendQueryParameter(KEY_PARAM, GOOGLE_API_KEY).build();

                return builtUri.toString();
            }
            case SEASON_DETAILS_TAG: {
                Uri builtUri = Uri.parse(OMDB_BASE_URL).buildUpon().
                        appendQueryParameter(TITLE_PARAM, title)
                        .appendQueryParameter(SEASON_PARAM, season).build();

                return builtUri.toString();
            }
            case EPISODE_DETAILS_TAG: {
                Uri builtUri = Uri.parse(OMDB_BASE_URL).buildUpon().
                        appendQueryParameter(TITLE_PARAM, title)
                        .appendQueryParameter(SEASON_PARAM, season)
                        .appendQueryParameter(EPISODE_PARAM, episode).build();
                Log.i(TAG, builtUri.toString());
                return builtUri.toString();
            }
        }
        return null;
    }

    /**
     * This method gets the HashKey for Facebook Android SDK
     * @param context context for packageManager
     */
    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.example.balazs.movieapp", PackageManager.GET_SIGNATURES);
            //Your            package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            Log.e(TAG, "error: " + e);
        }
    }
    /**
     * When the user clicks on Facebook share button this method is called. The method make a notification
     * with text and icon and with this notification takes the user back to the app MainActivity
     * @param context context for builder, intent and notificationManager
     */
    public static void facebookNotification(Context context) {
        NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
        notification.setAutoCancel(true);
        notification.setSmallIcon(R.drawable.ic_action_facebook);
        notification.setTicker(context.getString(R.string.notification_ticker));
        notification.setWhen(System.currentTimeMillis());
        notification.setContentText(context.getString(R.string.notification_text));
        notification.setContentTitle(context.getString(R.string.notification_title));

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID,notification.build());
    }

    /**
     * This method starts the Facebook share dialog with the url of the current movie, episode
     * or season
     *
     * @param imdbID   IMDb id for the url
     * @param activity
     */
    public static void startFacebookShareDialog(String imdbID, FragmentActivity activity) {
        ShareDialog mShareDialog = new ShareDialog(activity);
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(IMDB_BASE_URL + imdbID))
                .build();
        //fbShareButton.setShareContent(content);
        mShareDialog.show(content);

    }

    /**
     * This method shows the IMDb web page of the current movie, episode or season
     *
     * @param imdbID  IMDb id for the url
     * @param context context for the intent
     */
    public static void startImdbWebPage(String imdbID, Context context) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(IMDB_BASE_URL + imdbID));
        context.startActivity(i);
    }

    public static boolean isExternalStorageIsWriteable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static final int NOTIFICATION_ID = 40111;

    public static final String STRING_CASE_MAPS_ACTIVITY = "map";
    public static final String STRING_CASE_MOVIE_ACTIVITY = "movie";

    public static final int PROXIMITY_RADIUS = 5000;
    public static final String GOOGLE_API_KEY = "AIzaSyCwkXlJ2iYMIsuqgOXvrzvOS1OMjf7XH_I";

    public static final String CHOSEN_MOVIE_TAG = "CHOSEN_MOVIE_TAG";
    public static final String MOVIE_DETAILS_TAG = "MOVIE_DETAILS_TAG";
    public static final String SEASON_DETAILS_TAG = "SEASON_DETAILS_TAG";
    public static final String NEARBY_PLACES_TAG = "NEARBY_PLACES_TAG";
    public static final String EPISODE_DETAILS_TAG = "EPISODE_DETAILS_TAG";


    public static final String type = "json";
    public static final String plot = "full";
    public static final String sensor = "true";
    public static final int page = 1;

    public static final String KEY_MAPS_STATUS = "status";
    public static final String LOCATION_PARAM = "location";
    public static final String RADIUS_PARAM = "radius";
    public static final String MAP_TYPE_PARAM = "type";
    public static final String SENSOR_PARAM = "sensor";
    public static final String KEY_PARAM = "key";


    public static final String IMDB_BASE_URL = "http://www.imdb.com/title/";
    public static final String NEARBY_BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
    public final static String OMDB_BASE_URL = "http://www.omdbapi.com/?";
    public final static String TITLE_PARAM = "t";
    public final static String CHOSEN_TITLE_PARAM = "s";
    public final static String PAGE_PARAM = "page";

    public final static String PLOT_PARAM = "plot";
    public final static String TYPE_PARAM = "r";
    public final static String SEASON_PARAM = "season";
    public final static String EPISODE_PARAM = "episode";

    public static final String KEY_TOTALSEASON = "totalSeasons";


    public static final String KEY_RESPONSE = "Response";

    public static final String KEY_TITLE = "Title";
    public static final String KEY_RELEASED = "Released";
    public static final String KEY_RUNTIME = "Runtime";
    public static final String KEY_PLOT = "Plot";
    public static final String KEY_LANGUAGE = "Language";
    public static final String KEY_COUNTRY = "Country";
    public static final String KEY_POSTER = "Poster";
    public static final String KEY_IMDBRATING = "imdbRating";
    public static final String KEY_IMDBID = "imdbID";
    public static final String KEY_TYPE = "Type";
    public static final String KEY_SEASON = "Season";
    public static final String KEY_EPISODE = "Episode";


}
