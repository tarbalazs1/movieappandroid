package com.example.balazs.movieapp.autocomplete;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for creating, upgrading, reading database
 */
public class SQLiteAdapter extends SQLiteOpenHelper {

    private static final String TAG = SQLiteAdapter.class.getSimpleName();

    // database version
    private static final int DATABASE_VERSION = 4;

    // database name
    protected static final String DATABASE_NAME = "MovieTitle";

    // table details
    public String tableName = "title";
    public String fieldObjectId = "id";
    public String fieldObjectName = "name";

    // constructor
    public SQLiteAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Data table or tables are created
     * @param db SQLiteDatabase object
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "";

        sql += "CREATE TABLE " + tableName;
        sql += " ( ";
        sql += fieldObjectId + " INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += fieldObjectName + " TEXT ";
        sql += " ) ";

        db.execSQL(sql);

    }


    /**
     * When upgrading the database, it will drop the current table and recreate.
     * @param db database
     * @param oldVersion old version of the database
     * @param newVersion new version of the database
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "DROP TABLE IF EXISTS " + tableName;
        db.execSQL(sql);

        onCreate(db);
    }


    /**
     * create new record
     * @param myObj contains details to be added as single row.
     * @return true of false
     */
    public boolean create(SQLiteTitleObject myObj) {

        boolean createSuccessful = false;

        if (!checkIfExists(myObj.getTitleName())) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(fieldObjectName, myObj.getTitleName());
            createSuccessful = db.insert(tableName, null, values) > 0;

            db.close();

            if (createSuccessful) {
                Log.e(TAG, myObj.getTitleName() + " created.");
            }
        }

        return createSuccessful;
    }


    /**
     * Check if the record exists so it won't insert the next time you run this code
     * @param objectName analyzed String object
     * @return true or false
     */
    public boolean checkIfExists(String objectName) {

        boolean recordExists = false;

        objectName = objectName.replaceAll("'", "''");

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + fieldObjectId + " FROM " + tableName + " WHERE " +
                fieldObjectName + " = '" + objectName + "'", null);

        if (cursor != null) {

            if (cursor.getCount() > 0) {
                recordExists = true;
            }
        }

        assert cursor != null;
        cursor.close();
        db.close();

        return recordExists;
    }


    /**
     * Read records related to the search term, first the select query, than execute the query,
     * and looping trough all rows and adding to list
     * @return string array of records
     */
    public List<SQLiteTitleObject> read() {

        List<SQLiteTitleObject> recordsList = new ArrayList<>();

        // select query
        String sql = "";
        sql += "SELECT * FROM " + tableName;
        //sql += " WHERE " + fieldObjectName + " LIKE '%" + searchTerm + "%'";
        //sql += " ORDER BY " + fieldObjectId + " DESC";
       // sql += " LIMIT 0,5";

        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                // int productId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(fieldProductId)));
                String objectName = cursor.getString(cursor.getColumnIndex(fieldObjectName));
                SQLiteTitleObject myObject = new SQLiteTitleObject(objectName);

                // add to list
                recordsList.add(myObject);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

}