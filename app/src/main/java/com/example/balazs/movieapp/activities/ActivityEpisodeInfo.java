package com.example.balazs.movieapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.balazs.movieapp.R;

/**
 * Activity that contains the episode info fragment
 *
 * Created by Balázs on 16/03/27.
 */
public class ActivityEpisodeInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episode_info);
    }
}
