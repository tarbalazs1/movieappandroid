package com.example.balazs.movieapp.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.balazs.movieapp.classes.AsyncMovie;
import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.extras.Util;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Activity that contains the map and streetView fragments, find the user's location, handle
 * the nearby places based on the user's current location, and shows them on streetView
 */
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        OnStreetViewPanoramaReadyCallback, LocationListener, GoogleMap.OnMarkerClickListener,
        android.support.v4.app.LoaderManager.LoaderCallbacks<Object> {

    private static final String TAG = MapsActivity.class.getSimpleName();
    private StreetViewPanorama mStreetView;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private String mProvider;
    private static int mMapType;
    private String mUrl;
    private Marker myMarker;
    static double mLatitude;
    static double mLongitude;
    private LatLng mStreetViewLocation;
    boolean mAsyncBoolean;
    public static final int MAPS_ASYNC_ID = 3;
    private int mAsyncID;

    private ArrayList<HashMap<String, String>> mGooglePlacesList;
    private boolean mHaveLocation;

    /**
     * Called when the activity is starting. In this method I initialize some variables
     * @param savedInstanceState  If the activity is being re-initialized after previously being
     *                            shut down then this Bundle contains the data
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mMapType = 1;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mProvider = locationManager.getBestProvider(new Criteria(), true);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            mGooglePlacesList = (ArrayList<HashMap<String, String>>) savedInstanceState.getSerializable("Places");
            mAsyncBoolean = savedInstanceState.getBoolean("Async");
            mHaveLocation = savedInstanceState.getBoolean("Location");
            mAsyncID = savedInstanceState.getInt("AsyncID");
            mStreetViewLocation = savedInstanceState.getParcelable("StreetViewLocation");
        }

        if (mAsyncID != 0) {
            Log.i(TAG, "if else " + mAsyncID);

            getSupportLoaderManager().initLoader(mAsyncID, null, this).forceLoad();
        }
    }


    /**
     * Called when map is ready
     * @param googleMap GoogleMap object
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setPadding(getResources().getInteger(R.integer.map_compass_padding_left),
                getResources().getInteger(R.integer.map_compass_padding_top),
                getResources().getInteger(R.integer.map_compass_padding_right),
                getResources().getInteger(R.integer.map_compass_padding_bottom));

        LatLng zeroLocation = new LatLng(getResources().getInteger(R.integer.default_lat),
                getResources().getInteger(R.integer.default_lng));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(zeroLocation));

        Location location = null;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            location = locationManager.getLastKnownLocation(mProvider);
        }

        if (location != null) {
            onLocationChanged(location);
        }
        if (mGooglePlacesList != null) {
            //az oncreate-ba hívtam meg a markerek visszaállítását ami nem volt jó mert a map még nem volt ready
            nearbyMarkers();
        }

    }

    /**
     * Called when streetView is ready
     * @param streetViewPanorama StreetViewPanorama object
     */
    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        mStreetView = streetViewPanorama;
        streetViewPanorama.setPosition(new LatLng(mLatitude, mLongitude));
        if (mStreetViewLocation != null) {
            setStreetView(mStreetViewLocation);
        }
    }

    /**
     * When the user's location changed, this method is called, which removes the user's marker if it
     * exists and create the new marker with the current location
     *
     * @param location the data of our new location
     */
    @Override
    public void onLocationChanged(Location location) {
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        LatLng latLng = new LatLng(mLatitude, mLongitude);

        if (myMarker != null) {
            myMarker.remove();
        }

        MarkerOptions options = new MarkerOptions().position(latLng).title(getString(R.string.str_my_location)).
                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        myMarker = mMap.addMarker(options);


        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12));

        mHaveLocation = true;

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    protected void onPause() {
        super.onPause();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.removeUpdates(this);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(mProvider, 2000, 0, this);
        }

    }

    /**
     * This method starts downloading the details of the requested places, if the location is known
     *
     * @param type The type of the requested places
     */
    public void showPlaces(String type) {

        if (mHaveLocation) {
            mUrl = Util.urlBuilder(null, Util.NEARBY_PLACES_TAG, mLatitude, mLongitude, type, null, null);
            mAsyncID = MAPS_ASYNC_ID;
            if (!mAsyncBoolean) {
                mAsyncBoolean = true;
                getSupportLoaderManager().initLoader(MAPS_ASYNC_ID, null, this).forceLoad();
            } else {
                getSupportLoaderManager().restartLoader(MAPS_ASYNC_ID, null, this).forceLoad();

            }
            Log.i(TAG, mUrl);
        } else {
            Toast.makeText(MapsActivity.this, R.string.str_toast_waiting_location, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Create the options menu, inflate the menu xml file
     * @param menu The options menu in which you place your items
     * @return super call
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * This method is called when the user select an item from options menu
     * @param item The selected item
     * @return Super call
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_movie_rental:

                showPlaces(getString(R.string.str_movie_rental_json));
                return true;
            case R.id.action_restaurant:
                showPlaces(getString(R.string.str_restaurant_json));
                return true;
            case R.id.action_theater:
                showPlaces(getString(R.string.str_movie_theater_json));
                return true;

            case R.id.action_globe:
                if (mMapType <= 3) {
                    mMapType++;
                    mMap.setMapType(mMapType);
                } else {
                    mMapType = 1;
                    mMap.setMapType(mMapType);
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Instantiate and return a new Loader for the given ID
     * @param id The ID whose loader is to be created
     * @param args Any arguments supplied by the caller
     * @return Return a new Loader instance
     */
    @Override
    public android.support.v4.content.Loader<Object> onCreateLoader(int id, Bundle args) {
        return new AsyncMovie(this, mUrl);
    }

    /**
     * Called when a previously created loader has finished its load. If data is valid than call
     * nearbyMarkers method
     * @param loader the loader that has finished
     * @param data the data generated by the loader
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Object> loader, Object data) {
        if (data != null && loader.getId() == MAPS_ASYNC_ID && data instanceof ArrayList<?>) {
            mGooglePlacesList = new ArrayList<>();
            for (Object o : (ArrayList<?>) data) {
                if (o instanceof HashMap<?, ?>) {
                    try {
                        mGooglePlacesList.add((HashMap<String, String>) o);
                    } catch (Exception e) {
                        Log.e(TAG, "error: " + e);
                    }
                }
            }

            nearbyMarkers();
            mAsyncID = 0;
        }
    }

    /**
     * saveImage items when the activity destroyed and recreated
     * @param outState the bundle that contains the required variables, values
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("Places", mGooglePlacesList);
        //el kell tenni a booleant mert kül csak inicializálja az asyncot és az nem jó
        outState.putBoolean("Async", mAsyncBoolean);
        outState.putBoolean("Location", mHaveLocation);
        outState.putInt("AsyncID", mAsyncID);
        if (mStreetView.getLocation() != null) {
            outState.putParcelable("StreetViewLocation", new LatLng(mStreetView.getLocation().position.latitude,
                    mStreetView.getLocation().position.longitude));
        }
        super.onSaveInstanceState(outState);
    }

    /**
     * This method clears the entire map, and puts the requested markers to the right places and
     * the user's current location as well
     */
    private void nearbyMarkers() {
        mMap.clear();
        MarkerOptions markerOptions;
        HashMap<String, String> googlePlace;
        double lat, lng;
        String placeName, vicinity;
        LatLng latLng;

        for (int i = 0; i < mGooglePlacesList.size(); i++) {
            markerOptions = new MarkerOptions();
            googlePlace = mGooglePlacesList.get(i);
            lat = Double.parseDouble(googlePlace.get("lat"));
            lng = Double.parseDouble(googlePlace.get("lng"));
            placeName = googlePlace.get("place_name");
            vicinity = googlePlace.get("vicinity");
            latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);
            markerOptions.title(placeName + " : " + vicinity);
            mMap.addMarker(markerOptions);
        }

        mMap.addMarker(new MarkerOptions().position(new LatLng(mLatitude, mLongitude)).title(getString(R.string.str_my_location)).
                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Object> loader) {

    }


    /**
     * When the user clicks on one of the markers, the marker shows the info window and call the
     * setStreetView method with the position of the current marker
     *
     * @param marker selected marker from the map
     * @return return false, because when it is true the toolbar disappears
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        if (!marker.getTitle().equals(myMarker.getTitle())) {
            setStreetView(marker.getPosition());
        }
        return false; //ha true-val térek vissza elmegy a google toolbar
    }

    /**
     * This method calls the layoutHeightWidthPercent() method and set the position of the streetView
     * with a LatLng object
     * @param streetView the position of the selected marker
     */
    public void setStreetView(LatLng streetView) {

        if (mStreetView == null)
            return;

        layoutHeightWidthPercent();

        StreetViewPanoramaCamera.Builder builder = new StreetViewPanoramaCamera.Builder(mStreetView.getPanoramaCamera());
        builder.tilt(0.0f);
        builder.zoom(0.0f);
        builder.bearing(0.0f);
        mStreetView.animateTo(builder.build(), 1000);

        mStreetView.setPosition(streetView, 300);
        mStreetView.setStreetNamesEnabled(true);
        mStreetView.setPosition(streetView);
        /*LatLng latLng = new LatLng(46.0774914, 18.215414);
        mStreetView.setPosition(latLng);*/
    }

    /**
     * This method sets the different height and width for the two RelativeLayouts of this Activity,
     * based on the current orientation
     */
    private void layoutHeightWidthPercent() {
        RelativeLayout layout;
        PercentRelativeLayout.LayoutParams params;
        PercentLayoutHelper.PercentLayoutInfo info;
        float map = 0.60f;
        float streetView = 0.40f;

        layout = (RelativeLayout) findViewById(R.id.map_relative_layout);
        params = (PercentRelativeLayout.LayoutParams) layout.getLayoutParams();
        info = params.getPercentLayoutInfo();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.i(TAG, "portrait");
            info.heightPercent = map;
        } else {
            Log.i(TAG, "landscape");
            info.widthPercent = map;
        }

        layout.requestLayout();

        layout = (RelativeLayout) findViewById(R.id.streetview_relative_layout);
        params = (PercentRelativeLayout.LayoutParams) layout.getLayoutParams();
        // This will currently return null, if it was not constructed from XML.
        info = params.getPercentLayoutInfo();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.i(TAG, "portrait");
            info.heightPercent = streetView;
        } else {
            Log.i(TAG, "landscape");
            info.widthPercent = streetView;
        }
        layout.requestLayout();


        // This will currently return null, if it was not constructed from XML.

    }
}
