package com.example.balazs.movieapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.beans.Movie;

/**
 * Activity that contains the movie info fragment
 *
 * Created by Balázs on 16/03/08.
 */
public class ActivityMovieInfo extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_info);

    }

}
