package com.example.balazs.movieapp.autocomplete;

/**
 * This class defines the object for the sqlite database
 *
 * Created by Balázs on 16/03/14.
 */
public class SQLiteTitleObject {

    private String titleName;

    // constructor for adding sample data
    public SQLiteTitleObject(String titleName) {

        this.titleName = titleName;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }
}
