package com.example.balazs.movieapp.interfaces;

/**Communication between MovieTitleDialog and FragmentMovieChooser.
 * It lets the fragment know about the change in SQLite database
 * Created by Balázs on 16/03/14.
 */
public interface IDatabaseChange {
     void onDatabaseChanged(String title);
}
