package com.example.balazs.movieapp.autocomplete;

import android.text.Editable;
import android.text.TextWatcher;

import com.example.balazs.movieapp.fragments.FragmentMovieChooser;

/**
 * Created by Balazs on 8/12/2016.
 */
public class CustomAutoCompleteTextChangedListener implements TextWatcher {

    public static final String TAG = "CustomAutoCompleteTextChangedListener.java";
    FragmentMovieChooser context;

    public CustomAutoCompleteTextChangedListener(FragmentMovieChooser context){
        this.context = context;
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence userInput, int start, int before, int count) {


       /* FragmentMovieChooser fragmentMovieChooser = context;
        fragmentMovieChooser.textChange(userInput.toString());


        // if you want to see in the logcat what the user types
        Log.e(TAG, "User input: " + userInput);

        MainActivity mainActivity = ((MainActivity) context);

        // query the database based on the user input
        mainActivity.item = mainActivity.getItemsFromDb(userInput.toString());

        // update the adapater
        mainActivity.myAdapter.notifyDataSetChanged();
        mainActivity.myAdapter = new ArrayAdapter<>(mainActivity, android.R.layout.simple_dropdown_item_1line, mainActivity.item);
        mainActivity.myAutoComplete.setAdapter(mainActivity.myAdapter);
*/
    }

}