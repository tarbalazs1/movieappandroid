package com.example.balazs.movieapp.application;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.facebook.FacebookSdk;

import io.realm.Realm;
import io.realm.RealmConfiguration;


/**Class for maintaining global application state
 * Created by Balázs on 16/04/14.
 */
public class MovieApp extends MultiDexApplication {

    /**Set the base context for this ContextWrapper.
     * @param base The new base context for this wrapper.
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * Called when the application is starting, so here I set the RealmConfiguration, and Facebook
     * SDK initialization
     */
    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration configuration = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(configuration);

        FacebookSdk.sdkInitialize(this);

    }
}
