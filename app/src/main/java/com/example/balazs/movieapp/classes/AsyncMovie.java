package com.example.balazs.movieapp.classes;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.balazs.movieapp.classes.HTTPDataParser;
import com.example.balazs.movieapp.extras.Util;
import com.example.balazs.movieapp.handlers.HTTPDataHandler;

import org.json.JSONException;

import java.io.IOException;

/**
 * This class creates an asynchronous thread to handle the downloads according to its ID
 *
 * Created by Balázs on 16/03/15.
 */
public class AsyncMovie extends AsyncTaskLoader<Object> {

    private static final String TAG = AsyncMovie.class.getSimpleName();
    private String mURL;


    public AsyncMovie(Context context, String url) {
        super(context);
        this.mURL = url;
    }


    /**
     * Called on a worker thread to perform the actual load and to return the result of the load operation.
     * @return the downloaded and parsed data to the UI tread
     */
    @Override
    public Object loadInBackground() {
        if (mURL == null) {
            return null;
        }

        switch (getId()) {
            case 1: {
                String movieJsonStr;


                movieJsonStr = new HTTPDataHandler().getHTTPData(mURL);

                try {
                    if (movieJsonStr != null) {
                        return new HTTPDataParser(getContext()).getMovieDataFromJson(movieJsonStr, Util.MOVIE_DETAILS_TAG);
                    } else {
                        return null;
                    }

                } catch (JSONException | IOException e) {
                    Log.e(TAG, "error: " + e);
                }
            }
            case 2: {
                String movieListJsonStr;

                movieListJsonStr = new HTTPDataHandler().getHTTPData(mURL);

                try {
                    if (movieListJsonStr != null) {
                        //figyelni kell mert ha wifi üresjáraton megy vagy csak nem megy a névfeloldás vagy bármi
                        //akkor a null stringet próbálja feldolgozni és elrepül nullpointer
                        //lehet elég lenne a catch ágba egy nullpointer kezelés? hm de akkor is megpróbálná beparsolni itt hamarabb
                        //el tudom kapni
                        return new HTTPDataParser().getListDataFromJson(movieListJsonStr, Util.CHOSEN_MOVIE_TAG);
                    } else {
                        return null;
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "error: " + e);
                }
            }
            case 3: {
                String nearbyPlacesJsonStr;

                nearbyPlacesJsonStr = new HTTPDataHandler().getHTTPData(mURL);
                try {
                    if (nearbyPlacesJsonStr != null) {
                        return new HTTPDataParser().getPlacesFromJson(nearbyPlacesJsonStr);
                    } else {
                        return null;
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "error: " + e);
                }
            }
            case 4: {
                String seasonInfoJsonStr;

                seasonInfoJsonStr = new HTTPDataHandler().getHTTPData(mURL);
                try {
                    if (seasonInfoJsonStr != null) {
                        return new HTTPDataParser().getListDataFromJson(seasonInfoJsonStr, Util.SEASON_DETAILS_TAG);
                    } else {
                        return null;
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "error: " + e);
                }
            }
            case 5: {
                String episodeInfoJsonStr;

                episodeInfoJsonStr = new HTTPDataHandler().getHTTPData(mURL);
                try {
                    if (episodeInfoJsonStr != null) {
                        //itt is kell a kotextus mert lehet kell a kép és akkor arra repült el oh my..
                        return new HTTPDataParser(getContext()).getMovieDataFromJson(episodeInfoJsonStr, Util.EPISODE_DETAILS_TAG);
                    } else {
                        return null;
                    }
                } catch (JSONException | IOException e) {
                    Log.e(TAG, "error: " + e);
                }

            }

        }
        return null;

    }
}
