package com.example.balazs.movieapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.beans.Season;
import com.example.balazs.movieapp.fragments.FragmentSeasonInfo;
import com.example.balazs.movieapp.interfaces.ISeasonImage;

import java.util.List;

/**This class is a BaseAdapter for season list
 * Created by Balázs on 2015.10.30..
 */
public class SeasonListAdapter extends BaseAdapter {

    private static final String TAG = SeasonListAdapter.class.getSimpleName();
    private List<Season> mSeasonList;
    private LayoutInflater mInflater;
    private ISeasonImage mISeasonImage;

    public SeasonListAdapter(Context context, List<Season> season, FragmentSeasonInfo fragmentSeasonInfo) {
        mSeasonList = season;
        mInflater = LayoutInflater.from(context);
        mISeasonImage = fragmentSeasonInfo;
    }

    public void addItem(Season info) {
        mSeasonList.add(info);
    }

    /**How many items are in the data set represented by this Adapter
     * @return size of season list
     */
    @Override
    public int getCount() {
        return mSeasonList.size();
    }

    /**Get the data item associated with the specified position in the data set
     * @param position  Position of the item whose data we want within the adapter's data set
     * @return return the data at the specified position
     */
    @Override
    public Season getItem(int position) {
        return mSeasonList.get(position);
    }

    /**Get the row id associated with the specified position in the list
     * @param position The position of the item within the adapter's data set whose row id we want
     * @return The id of the item at the specified position
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**Get a View that displays the data at the specified position in the data set
     * @param position  The position of the item within the adapter's data set of the item whose view we want
     * @param convertView  The old view to reuse, if possible
     * @param parent The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.season_list_row, parent, false);
        }

        Season season = mSeasonList.get(position);


        TextView tvEpisodeTitle = (TextView) convertView.findViewById(R.id.listMovieTitle);
        tvEpisodeTitle.setText(Html.fromHtml(convertView.getResources().getString(R.string.html_details_start_tag)
                + season.getEpisodeTitle() + convertView.getResources().getString(R.string.html_details_end_tag)));

        TextView tvEpisodeReleased = (TextView) convertView.findViewById(R.id.movieReleaseDate);
        tvEpisodeReleased.setText(season.getEpisodeReleased());

        RatingBar movieAudienceScore = (RatingBar) convertView.findViewById(R.id.movieAudienceScore);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.movieThumbnail);
        imageView.setOnClickListener(new ImageOnclickListener(position));
        //lehet N/A az eredmény itt el is szállt
        try {
            float ratingimdb = Float.valueOf(season.getEpisodeImdbRating());
            movieAudienceScore.setRating((ratingimdb / 2.0F));
        } catch (NumberFormatException e) {
            Log.e(TAG, "error: " + e);

            movieAudienceScore.setRating(0);
        }


        return convertView;
    }

    /**
     * Class that handle image click event
     */
    private class ImageOnclickListener implements View.OnClickListener {
        private int mPosition;

        public ImageOnclickListener(int position) {
            mPosition = position;
        }

        /**This method called when a view (IMDb icon) has been clicked
         * @param v  The view that was clicked
         */
        @Override
        public void onClick(View v) {
            mISeasonImage.onEpisodeImdbClick(mPosition);
        }
    }
}

