package com.example.balazs.movieapp.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.activities.ActivityMovieChooser;
import com.example.balazs.movieapp.activities.ActivityMovieInfo;
import com.example.balazs.movieapp.adapters.AdapterTitle;
import com.example.balazs.movieapp.adapters.Divider;
import com.example.balazs.movieapp.adapters.SimpleTouchCallback;
import com.example.balazs.movieapp.autocomplete.CustomAutoCompleteTextChangedListener;
import com.example.balazs.movieapp.autocomplete.CustomAutoCompleteView;
import com.example.balazs.movieapp.autocomplete.SQLiteAdapter;
import com.example.balazs.movieapp.autocomplete.SQLiteTitleObject;
import com.example.balazs.movieapp.beans.Movie;
import com.example.balazs.movieapp.beans.Title;
import com.example.balazs.movieapp.classes.AsyncMovie;
import com.example.balazs.movieapp.extras.Util;
import com.example.balazs.movieapp.handlers.AsyncTaskLoaderHandler;
import com.example.balazs.movieapp.interfaces.IDatabaseChange;
import com.example.balazs.movieapp.interfaces.TitleClickListener;
import com.example.balazs.movieapp.widget.TitleRecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Balázs on 16/03/07.
 */
public class FragmentMovieChooser extends Fragment implements View.OnClickListener, RealmChangeListener,
        TitleClickListener, IDatabaseChange, LoaderManager.LoaderCallbacks<Object> {

    private static final String TAG = FragmentMovieChooser.class.getSimpleName();
    private static final int MOVIE_INFO_TASK_ID = 1;
    private static final int CHOSEN_TITLE_TASK_ID = 2;
    private boolean mAsyncBoolean;

    private int mAsyncID;

    private final int SPEECH_RECOGNITION_CODE = 1;

    private Toolbar mToolbar;

    private Button mBtnChooser;

    private String mMovieTitle;

    private TitleRecyclerView mRecycler;
    private Realm mRealm;
    private RealmResults<Title> mResults;
    private AdapterTitle mAdapter;
    private TextView mMovieList;
    private View mEmptyView;

    private SQLiteAdapter mSQLiteAdapter;
    private AutoCompleteTextView mAutoCompleteTextView;
    private AutoCompleteTextView mCustomAutoCompleteView;
    private ArrayAdapter<String> mArrayAdapter;
    private String[] mItem;
    private String mURL;

    /**
     * Lifecycle method creates and returns the view hierarchy associated with the fragment
     *
     * @param inflater           LayoutInflater object that can be used to inflate any views
     * @param container          The parent view
     * @param savedInstanceState bundle that used for return the saved data
     * @return Return the View for the fragment's UI
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_chooser, container, false);
        setHasOptionsMenu(true);

        mCustomAutoCompleteView = (AutoCompleteTextView) rootView.findViewById(R.id.et_title);
        mSQLiteAdapter = new SQLiteAdapter(getActivity());
        mItem = new String[]{"test"};


        // mHandler = new AsyncTaskLoaderHandler();
        mToolbar = (Toolbar) rootView.findViewById(R.id.tb_toolbar);
        ((ActivityMovieChooser) getActivity()).setSupportActionBar(mToolbar);
        mRealm = Realm.getDefaultInstance();
        mResults = mRealm.where(Title.class).findAllAsync();
        mEmptyView = rootView.findViewById(R.id.empty_title);
        mMovieList = (TextView) rootView.findViewById(R.id.tv_search);

        mBtnChooser = (Button) rootView.findViewById(R.id.btn_ok);
        mRecycler = (TitleRecyclerView) rootView.findViewById(R.id.rv_title);
        mRecycler.addItemDecoration(new Divider(getActivity(), LinearLayoutManager.VERTICAL));


        mRecycler.showIfEmpty(mEmptyView);
        mRecycler.hideIfEmpty(mMovieList);

        mAdapter = new AdapterTitle(getActivity(), mRealm, mResults, this);
        mAdapter.setHasStableIds(true);

        mRecycler.setAdapter(mAdapter);
        SimpleTouchCallback sImpleTouchCallback = new SimpleTouchCallback(mAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(sImpleTouchCallback);
        helper.attachToRecyclerView(mRecycler);

        mBtnChooser.setOnClickListener(this);

        // insertSampleData();

        onDatabaseChanged(null);

        mCustomAutoCompleteView.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));

        mArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.list_view_row_item,
                R.id.tv_autocomplete_item, mItem);
        mCustomAutoCompleteView.setAdapter(mArrayAdapter);

        mCustomAutoCompleteView.setThreshold(1);
        mCustomAutoCompleteView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                writeToRealm(parent.getItemAtPosition(position).toString());
                mCustomAutoCompleteView.setText("");
                Log.i(TAG, parent.getItemAtPosition(position).toString());

                hideKeyboard();
            }
        });

        textChange();
        if (savedInstanceState != null) {
            mAsyncBoolean = savedInstanceState.getBoolean("Async");
            mAsyncID = savedInstanceState.getInt("AsyncID");
        }

        if (mAsyncID != 0) {
            Log.i(TAG, "if else " + mAsyncID);

            getActivity().getSupportLoaderManager().initLoader(mAsyncID, null, this).forceLoad();
        }
        return rootView;
    }

    /**
     * This method hides the keyboard
     */
    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * This method writes the selected title to the Realm database
     *
     * @param titlePos selected title
     */
    private void writeToRealm(String titlePos) {
        long now = System.currentTimeMillis();
        Title title = new Title(titlePos, now);
        Realm realm = Realm.getDefaultInstance();
        if (!checkIfExists(realm, title.getMovieTitle())) {
            realm.beginTransaction();
            realm.copyToRealm(title);
            //itt ment a hiba mert nem commitoltam a tranzakciot
            realm.commitTransaction();
            realm.close();
        }
    }

    /**
     * This method checks the selected title if it exists or not
     *
     * @param realm      Realm object
     * @param movieTitle movie title string
     * @return true or false according to the result
     */
    private boolean checkIfExists(Realm realm, String movieTitle) {

        RealmQuery<Title> query = realm.where(Title.class)
                .equalTo("movieTitle", movieTitle);

        return query.count() != 0;
    }

    private void insertSampleData() {
        // CREATE
        mSQLiteAdapter.create(new SQLiteTitleObject("Supernatural"));
        mSQLiteAdapter.create(new SQLiteTitleObject("The Walking Dead"));
        mSQLiteAdapter.create(new SQLiteTitleObject("Up"));
        mSQLiteAdapter.create(new SQLiteTitleObject("Suits"));
    }


    /**
     * Called when OK button has been clicked
     *
     * @param v The view that was clicked
     */
    @Override
    public void onClick(View v) {

        if (Util.isNetworkAvailable(getActivity())) {
            if (mCustomAutoCompleteView.getText().toString().equals("")) {
                Toast.makeText(getActivity(), R.string.str_toast_empty_field, Toast.LENGTH_SHORT).show();

            } else if (mCustomAutoCompleteView.getText().toString().length() == 1) {
                Toast.makeText(getActivity(), R.string.str_toast_more_than_one_char, Toast.LENGTH_SHORT).show();
            } else {
                mMovieTitle = mCustomAutoCompleteView.getText().toString();
                mCustomAutoCompleteView.setText("");
                downloadMovieList();
            }
        } else {
            Toast.makeText(getActivity(), R.string.str_toast_no_internet, Toast.LENGTH_SHORT).show();
        }
        hideKeyboard();
    }

    /**
     * This method set the url and start the loader to download the title list
     */
    private void downloadMovieList() {
        mURL = Util.urlBuilder(mMovieTitle.replaceAll(" ", "+"), Util.CHOSEN_MOVIE_TAG, 0, 0, null, null, null);
        Log.i(TAG, mURL);
        mAsyncID = CHOSEN_TITLE_TASK_ID;
        if (!mAsyncBoolean) {
            mAsyncBoolean = true;
            getActivity().getSupportLoaderManager().initLoader(CHOSEN_TITLE_TASK_ID, null, this).forceLoad();
        } else {
            getActivity().getSupportLoaderManager().restartLoader(CHOSEN_TITLE_TASK_ID, null, this).forceLoad();
        }
    }

    /**
     * Called when a Realm transaction is committed.
     */
    @Override
    public void onChange() {
        mAdapter.update(mResults);
    }

    /**
     * Set the Realm listener
     */
    @Override
    public void onStart() {
        super.onStart();
        mResults.addChangeListener(this);
    }

    /**
     * Remove the Realm listener
     */
    @Override
    public void onStop() {
        super.onStop();
        mResults.removeChangeListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * This hook is called whenever an item (sort a-z sort z-a microphone) in my options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_sort_az:
                onSortZa();
                return true;
            case R.id.action_sort_za:
                onSortAz();
                return true;
            case R.id.action_mic:
                startSpeechToText();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Start speech to text intent. This opens up Google Speech Recognition API dialog box to listen the speech input.
     */

    private void startSpeechToText() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getResources().getString(R.string.str_recognizer));
        try {
            startActivityForResult(intent, SPEECH_RECOGNITION_CODE);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "error: " + e);
            Toast.makeText(getActivity(),
                    R.string.str_toast_speech_rec_error,
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Callback for speech recognition activity
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SPEECH_RECOGNITION_CODE: {
                if (resultCode == 0xffffffff && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    mMovieTitle = result.get(0);
                    downloadMovieList();

                }
                break;
            }

        }
    }


    /**
     * Realm sort method descending
     */
    public void onSortAz() {
        mResults = mRealm.where(Title.class).findAllSortedAsync("movieTitle", Sort.DESCENDING);
        mResults.addChangeListener(this);
    }

    /**
     * Realm sort method ascending
     */
    public void onSortZa() {
        mResults = mRealm.where(Title.class).findAllSortedAsync("movieTitle");
        mResults.addChangeListener(this);
    }

    /**
     * Called when the user clicked the details button in RecyclerView
     *
     * @param position position in RecyclerView
     */
    @Override
    public void onTitleClick(int position) {
        if (Util.isNetworkAvailable(getActivity())) {
            mURL = Util.urlBuilder(mResults.get(position).getMovieTitle().replaceAll(" ", "+"),
                    Util.MOVIE_DETAILS_TAG, 0, 0, null, null, null);
            Log.i(TAG, mURL);
            /*
            Intent i = new Intent(getActivity(), ActivityMovieInfo.class);
            startActivity(i);*/
            mAsyncID = MOVIE_INFO_TASK_ID;
            if (!mAsyncBoolean) {
                mAsyncBoolean = true;
                getActivity().getSupportLoaderManager().initLoader(MOVIE_INFO_TASK_ID, null, this).forceLoad();
            } else {
                getActivity().getSupportLoaderManager().restartLoader(MOVIE_INFO_TASK_ID, null, this).forceLoad();
            }
        }
    }

    /**
     * Called form onCreate() method and when the database was changed
     */
    @Override
    public void onDatabaseChanged(String title) {
        //getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        if (title != null) {
            mSQLiteAdapter.create(new SQLiteTitleObject(title));
            textChange();
        }

    }

    /**
     * Instantiate and return a new Loader for the given ID.
     *
     * @param id   The ID whose loader is to be created
     * @param args Any arguments supplied by the caller
     * @return
     */
    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new AsyncMovie(getActivity(), mURL);
    }

    /**
     * Called when a previously created loader has finished its load.
     *
     * @param loader The Loader that has finished.
     * @param data   The data generated by the Loader. It can be a Movie object or an ArrayList of titles
     */
    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        if (data != null && loader.getId() == MOVIE_INFO_TASK_ID && data instanceof Movie) {
            startMovieInfoPage((Movie) data);

        } else if (data != null && loader.getId() == CHOSEN_TITLE_TASK_ID && data instanceof ArrayList<?>) {
            ArrayList<String> chosenMovieList = new ArrayList<>();
            for (Object o : (ArrayList<?>) data) {
                if (o instanceof String) {
                    chosenMovieList.add((String) o);
                }
            }
            AsyncTaskLoaderHandler loaderHandler = new AsyncTaskLoaderHandler(this, chosenMovieList);
            loaderHandler.sendEmptyMessage(2);
        } else {
            Toast.makeText(getActivity(), "No movie was found \"" + mMovieTitle + "\"", Toast.LENGTH_SHORT).show();
        }
        mAsyncID = 0;
    }


    /**
     * Start the ActivityMovieInfo Activity
     *
     * @param data Movie object with data that the user want to see
     */
    private void startMovieInfoPage(Movie data) {
        Intent i = new Intent(getActivity(), ActivityMovieInfo.class);
        Log.i(TAG, data.getTitle());
        i.putExtra("MOVIE", data);
        startActivity(i);
    }

    /**
     * Called when a previously created loader is being reset, and thus making its data unavailable.
     *
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    /**
     * Called to retrieve per-instance state from an activity before being killed so that the
     * state can be restored in onCreate(Bundle) or onRestoreInstanceState(Bundle)
     *
     * @param outState Bundle in which to place my saved state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("Async", mAsyncBoolean);
        outState.putInt("AsyncID", mAsyncID);
        super.onSaveInstanceState(outState);
    }

    public void textChange() {

        // query the database based on the user input
        //mItem=null;
        mItem = getItemsFromDb();

        // update the adapater
        mArrayAdapter.notifyDataSetChanged();
        mArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.list_view_row_item,
                R.id.tv_autocomplete_item, mItem);
        mCustomAutoCompleteView.setAdapter(mArrayAdapter);
    }

    // this function is used in CustomAutoCompleteTextChangedListener.java
    public String[] getItemsFromDb() {

        // add items on the array dynamically
        List<SQLiteTitleObject> products = mSQLiteAdapter.read();
        int rowCount = products.size();

        String[] item = new String[rowCount];
        int x = 0;

        for (SQLiteTitleObject record : products) {

            item[x] = record.getTitleName();
            x++;
        }

        return item;
    }
}
