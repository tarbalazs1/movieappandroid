package com.example.balazs.movieapp.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.beans.Episode;
import com.example.balazs.movieapp.beans.Movie;
import com.example.balazs.movieapp.classes.SaveImage;
import com.example.balazs.movieapp.extras.Util;

/**
 * This is a fragment class that shows the details of the current episode
 * Created by Balázs on 16/03/27.
 */
public class FragmentEpisodeInfo extends Fragment implements View.OnClickListener, OnLongClickListener {

    private ImageView mPoster;
    private TextView mTitle;
    private TextView mRuntime;
    private TextView mReleased;
    private TextView mCountry;
    private TextView mImdbRating;
    private TextView mLanguage;
    private TextView mPlot;
    private Episode mEpisode;
    private ImageView mFullscreenPoster;
    private boolean mIsVisible;


    /**
     * Lifecycle method creates and returns the view hierarchy associated with the fragment
     *
     * @param inflater           LayoutInflater object that can be used to inflate any views
     * @param container          The parent view
     * @param savedInstanceState bundle that used for return the saved data
     * @return Return the View for the fragment's UI
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_episode_info, container, false);

        mRuntime = (TextView) rootView.findViewById(R.id.tv_episode_runtime);
        mReleased = (TextView) rootView.findViewById(R.id.tv_episode_released);
        mImdbRating = (TextView) rootView.findViewById(R.id.tv_episode_imdbrating);
        mLanguage = (TextView) rootView.findViewById(R.id.tv_episode_language);
        mPlot = (TextView) rootView.findViewById(R.id.tv_episode_plot);
        mCountry = (TextView) rootView.findViewById(R.id.tv_episode_country);
        mFullscreenPoster = (ImageView) rootView.findViewById(R.id.iv_episode_fullscreen_poster);
        mTitle = (TextView) rootView.findViewById(R.id.tv_episode_title);

        mPoster = (ImageView) rootView.findViewById(R.id.iv_episode_poster);


        ImageButton mFacebookShare = (ImageButton) rootView.findViewById(R.id.ib_episode_facebook);
        ImageButton mIMDbLink = (ImageButton) rootView.findViewById(R.id.ib_episode_imdb);

        mFacebookShare.setOnClickListener(this);
        mIMDbLink.setOnClickListener(this);
        mPoster.setOnClickListener(this);
        mFullscreenPoster.setOnClickListener(this);
        mFullscreenPoster.setOnLongClickListener(this);

        if (savedInstanceState != null) {
            mIsVisible = savedInstanceState.getBoolean("Visible");
        }

        if (mIsVisible) {
            showFullScreenPoster();
        }
        return rootView;
    }

    /**
     * When the activity is created we can get the Episode object
     *
     * @param savedInstanceState bundle for savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEpisode = getActivity().getIntent().getParcelableExtra("EPISODE");
        setEpisodeData();
    }

    /**
     * Set the details of the current episode with some html tags for better UI/UX
     */
    private void setEpisodeData() {

        mTitle.setText(Html.fromHtml(getResources().getString(R.string.html_title_start_tag) + mEpisode.getTitle() +
                getResources().getString(R.string.str_ep_season_letter) + mEpisode.getSeason()
                + getResources().getString(R.string.str_ep_episode_letter)
                + mEpisode.getEpisode() + getResources().getString(R.string.html_title_end_tag)));

        mRuntime.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_runtime) +
                getResources().getString(R.string.html_details_end_tag) + mEpisode.getRuntime()));

        mReleased.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_released) +
                getResources().getString(R.string.html_details_end_tag) + mEpisode.getReleased()));

        mImdbRating.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_imdb_rating) +
                getResources().getString(R.string.html_details_end_tag) + mEpisode.getImdbRating()));

        mLanguage.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_language) +
                getResources().getString(R.string.html_details_end_tag) + mEpisode.getLanguage()));

        mPlot.setText(mEpisode.getPlot());

        mCountry.setText(Html.fromHtml(getResources().getString(R.string.html_details_start_tag) +
                getResources().getString(R.string.str_country) +
                getResources().getString(R.string.html_details_end_tag) + mEpisode.getCountry()));

        mPoster.setImageBitmap(Episode.getPoster());
        mFullscreenPoster.setImageBitmap(Episode.getPoster());
    }


    /**This method called when a view (facebook or imdb button) has been clicked
     * @param v  The view that was clicked
     */
    @Override
    public void onClick(View v) {
        if (Util.isNetworkAvailable(getContext())) {
            switch (v.getId()) {
                case R.id.ib_episode_facebook: {
                    Util.startFacebookShareDialog(mEpisode.getImdbID(), getActivity());
                    Util.facebookNotification(getContext());
                    break;
                }
                case R.id.ib_episode_imdb: {
                    Util.startImdbWebPage(mEpisode.getImdbID(), getContext());
                    break;
                }
                case R.id.iv_episode_poster: {
                    showFullScreenPoster();
                }
            }
        } else {
            Toast.makeText(getContext(), R.string.str_toast_no_internet, Toast.LENGTH_SHORT).show();
        }

        if (v.getId() == R.id.iv_episode_fullscreen_poster) {
            mFullscreenPoster.setVisibility(View.GONE);
            mIsVisible = false;
        }
    }

    /**
     * This method shows a fragment with the current poster
     */
    private void showFullScreenPoster() {
        if (mFullscreenPoster.getVisibility() == View.GONE) {
            mFullscreenPoster.setVisibility(View.VISIBLE);
            mIsVisible = true;
        }
    }

    /**
     * Called to retrieve per-instance state from an activity before being killed so that the
     * state can be restored in onCreate(Bundle) or onRestoreInstanceState(Bundle)
     * it holds the mIsVisible variable
     * @param outState Bundle in which to place my saved state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("Visible", mIsVisible);
        super.onSaveInstanceState(outState);
    }

    /**Called when the ImageView has been clicked and held.
     * @param v The view that was clicked and held.
     * @return true if the callback consumed the long click, false otherwise.
     */
    @Override
    public boolean onLongClick(View v) {
        permissionForStorage();
        return true;
    }
    private void permissionForStorage() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        }else{
            if (mFullscreenPoster.getVisibility() == View.VISIBLE && Util.isExternalStorageIsWriteable()) {
                SaveImage saveImage = new SaveImage();
                saveImage.saveImage(getContext(), Movie.getPoster(), mEpisode.getTitle().replaceAll(" ", ""));
            }
        }
    }
}
