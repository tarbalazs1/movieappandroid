package com.example.balazs.movieapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.beans.Title;
import com.example.balazs.movieapp.interfaces.SwipeListener;
import com.example.balazs.movieapp.interfaces.TitleClickListener;

import io.realm.Realm;
import io.realm.RealmResults;

/**This class is an Adapter for RecyclerView
 * Created by Balázs on 16/03/07.
 */
public class AdapterTitle extends RecyclerView.Adapter<AdapterTitle.TitleHolder> implements SwipeListener {

    private LayoutInflater mInflater;
    private RealmResults<Title> mResults;
    private Realm mRealm;
    private TitleClickListener mClickListener;

    public AdapterTitle(Context context, Realm realm, RealmResults<Title> results, TitleClickListener titleClickListener) {
        mInflater = LayoutInflater.from(context);
        mRealm = realm;
        mClickListener = titleClickListener;
        update(results);
    }

    /**This method is called when the Realm database was changed
     * @param results List of Realm results
     */
    public void update(RealmResults<Title> results) {
        mResults = results;
        notifyDataSetChanged();

    }

    /**Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item
     * @param parent The ViewGroup into which the new View will be added after it is bound to an adapter position
     * @param viewType The view type of the new View
     * @return A new TitleHolder that holds a View of the given view type
     */
    @Override
    public TitleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_title, parent, false);
        return new TitleHolder(view, mClickListener);
    }

    /**Called by RecyclerView to display the title at the specified position
     * @param holder The TitleHolder
     * @param position The position of the item within the adapter's data set
     */
    @Override
    public void onBindViewHolder(TitleHolder holder, int position) {
        Title title = mResults.get(position);
        holder.mTextTitle.setText(title.getMovieTitle());
    }

    /**Return the stable ID for the item at position, or NO_ID (-1)
     * @param position Adapter position to query
     * @return 	the stable ID of the item at position
     */
    @Override
    public long getItemId(int position) {
        if (position < mResults.size()) {
            return mResults.get(position).getAdded();
        }
        return RecyclerView.NO_ID;
    }

    /**Returns the total number of items in the data set hold by the adapter.
     * @return number of items
     */
    @Override
    public int getItemCount() {
        return mResults.size();
    }

    /**This method is called when the user swipes an item which means that item needs to be deleted.
     * It removes the swiped item from Realm database and notify the RecyclerView about it
     * @param position position in database
     */
    @Override
    public void onSwipe(int position) {
        mRealm.beginTransaction();
        mResults.get(position).removeFromRealm();
        mRealm.commitTransaction();
        notifyItemRemoved(position);
    }

    /**
     * This class represents the ViewHolder with OnClickListener
     */
    public static class TitleHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView mTextTitle;
        TitleClickListener mClickListener;
        Button mBtnDetails;

        public TitleHolder(View itemView, TitleClickListener titleClickListener) {
            super(itemView);
            mBtnDetails = (Button) itemView.findViewById(R.id.btn_row_details);
            mTextTitle = (TextView) itemView.findViewById(R.id.tv_title);
            mClickListener = titleClickListener;

            mBtnDetails.setOnClickListener(this);
        }

        /**
         * @param v
         */
        @Override
        public void onClick(View v) {
            mClickListener.onTitleClick(getAdapterPosition());
        }
    }
}
