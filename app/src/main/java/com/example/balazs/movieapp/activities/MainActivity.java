package com.example.balazs.movieapp.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.extras.Util;
import com.example.balazs.movieapp.fragments.MainFragment;
import com.example.balazs.movieapp.interfaces.ICommunicator;
import com.facebook.FacebookSdk;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Activity that contains the main fragment, and the navigation drawer fragment
 *
 * Created by Balázs on 16/02/23.
 */
public class MainActivity extends AppCompatActivity
        implements ICommunicator, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private String[] urls;
    private static final int REQUEST_LOCATION = 99;


    /**
     * Lifecycle method
     * @param savedInstanceState It's a bundle that used for return the saved data after the activity destroyed
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Util.showHashKey(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //setdrawerlistener ddepricated
        assert drawer != null;
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        urls = getResources().getStringArray(R.array.nav_urls);
        //Add the Very First i.e Squad Fragment to the Container
        MainFragment mainFragment = new MainFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.containerView, mainFragment, null);
        fragmentTransaction.commit();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * This method starts the new activity according to the tag variable
     * @param tag it is the tag from MainFragment that distinguish the different activities
     */
    @Override
    public void onModeSelected(String tag) {
        Intent i;
        switch (tag) {
            case Util.STRING_CASE_MAPS_ACTIVITY:
                permissionForLocation();
                break;
            case Util.STRING_CASE_MOVIE_ACTIVITY:
                i = new Intent(this, ActivityMovieChooser.class);
                startActivity(i);
                break;
        }
    }

    /**
     * This method checks the permission, because in Android 6.0
     * users grant permissions to apps while the app is running, not when they install the app.
     */
    private void permissionForLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        }else{
            startMapsActivity();
        }
    }

    /**
     * Start the MapsActivity
     */
    private void startMapsActivity() {
        Intent i = new Intent(this, MapsActivity.class);
        startActivity(i);
    }

    /**
     * This overridden method checks if the required permission was granted or not
     * according to the requestCode. If request is cancelled, the result arrays are empty.
     * @param requestCode request code for permission
     * @param permissions requested permission
     * @param grantResults integer array for granted permission
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission granted");
                    startMapsActivity();
                } else {

                    Toast.makeText(MainActivity.this, R.string.str_toast_permission_denied, Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "Permission denied");
                }
            }

        }
    }

    /**
     * This overridden method is called when the activity has detected the user's press of the back key
     * If the drawer is open, than close it, if not than super call
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * This method is called when the user select an item from the navigation drawer
     * @param item Selected item
     * @return true
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_youtube) {
            navDrawerAction(0);

        } else if (id == R.id.nav_imdb) {
            navDrawerAction(1);

        } else if (id == R.id.nav_hetediksor) {
            navDrawerAction(2);

        } else if (id == R.id.nav_moviescom) {
            navDrawerAction(3);

        } else if (id == R.id.nav_rotten) {
            navDrawerAction(4);

        } else if (id == R.id.nav_send) {
            navDrawerAction(5);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * This method is used for handle the navigation view item clicks
     * @param i int that represents the items
     */
    private void navDrawerAction(int i) {
        Intent intent;
        if (i != 5 && Util.isNetworkAvailable(this)) {

            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urls[i]));
            startActivity(intent);
        } else {

            intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(getString(R.string.str_mailto_scheme),
                    getString(R.string.str_mail_address), null));
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.str_mail_subject));
            startActivity(Intent.createChooser(intent, getString(R.string.str_mail_header)));
        }
    }
}

