package com.example.balazs.movieapp.classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.example.balazs.movieapp.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * This class saves the chosen posters to external storage
 * Created by Balázs on 16/04/22.
 */
public class SaveImage {
    private Context mContext;
    private final String TAG = SaveImage.class.getSimpleName();

    /**Called when the user want to save the chosen image. It makes a JPG and save it to te
     * public directory.
     * @param context Context for Toast messages
     * @param imageToSave The chosen image
     * @param title Title for the file name
     */
    public void saveImage(Context context, Bitmap imageToSave, String title) {
        mContext = context;
        Bitmap bitmap = imageToSave;
        OutputStream outputStream;
        File filePath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), title + ".jpg");
        try {
            outputStream = new FileOutputStream(filePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
            ableToSave();
        } catch (IOException e) {
            Log.e(TAG, "Error: " + e);
            unableToSave();
        }


    }


    /**
     * Toast for the user
     */
    private void unableToSave() {
        Toast.makeText(mContext, R.string.toast_picture_unsaved, Toast.LENGTH_SHORT).show();

    }

    /**
     * Toast for the user
     */
    private void ableToSave() {
        Toast.makeText(mContext, R.string.toast_picture_saved, Toast.LENGTH_SHORT).show();

    }
}
