package com.example.balazs.movieapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.beans.Season;

import java.util.List;

/**
 * Activity that contains the season info fragment
 */
public class ActivitySeasonInfo extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season_info);

    }

}
