package com.example.balazs.movieapp.beans;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * This class is a simple bean to store the details of an episode
 *
 * Created by Balázs on 16/03/27.
 */
public class Episode implements Parcelable {

    private String title;
    private String released;
    private String country;
    private String imdbRating;
    private String language;
    private String runtime;
    private String plot;
    private String season;
    private String episode;
    private String posterUrl;
    private String imdbID;
    private static Bitmap poster;

    public Episode(String title, String released, String country, String imdbRating,
                   String language, String runtime, String plot, String season, String episode, String posterUrl, String imdbID) {
        this.title = title;
        this.released = released;
        this.country = country;
        this.imdbRating = imdbRating;
        this.language = language;
        this.runtime = runtime;
        this.plot = plot;
        this.season = season;
        this.episode = episode;
        this.posterUrl = posterUrl;
        this.imdbID = imdbID;
    }

    protected Episode(Parcel in) {
        title = in.readString();
        released = in.readString();
        country = in.readString();
        imdbRating = in.readString();
        language = in.readString();
        runtime = in.readString();
        plot = in.readString();
        season = in.readString();
        episode = in.readString();
        posterUrl = in.readString();
        imdbID = in.readString();
    }

    public static final Creator<Episode> CREATOR = new Creator<Episode>() {
        @Override
        public Episode createFromParcel(Parcel in) {
            return new Episode(in);
        }

        @Override
        public Episode[] newArray(int size) {
            return new Episode[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getReleased() {
        return released;
    }

    public String getCountry() {
        return country;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public String getLanguage() {
        return language;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getPlot() {
        return plot;
    }

    public String getSeason() {
        return season;
    }

    public String getEpisode() {
        return episode;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getImdbID() {
        return imdbID;
    }

    public static Bitmap getPoster() {
        return poster;
    }

    public static void setPoster(Bitmap poster) {
        Episode.poster = poster;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(released);
        dest.writeString(country);
        dest.writeString(imdbRating);
        dest.writeString(language);
        dest.writeString(runtime);
        dest.writeString(plot);
        dest.writeString(season);
        dest.writeString(episode);
        dest.writeString(posterUrl);
        dest.writeString(imdbID);
    }
}