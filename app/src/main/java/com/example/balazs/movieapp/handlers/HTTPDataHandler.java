package com.example.balazs.movieapp.handlers;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**This class handles with downloads
 * Created by Balázs on 2016.03.10..
 */
public class HTTPDataHandler {

    private final static int SET_READ_TIMEOUT = 10000;/* milliseconds */
    private final static int SET_CONNECT_TIMEOUT = 15000;/* milliseconds */
    private static final String TAG = HTTPDataHandler.class.getSimpleName();

    public HTTPDataHandler() {
    }


    /**This method does the actual work, it downloads the Json data from url
     * @param urlString url address with Json data
     * @return Return the String data from the specified url
     */
    public String getHTTPData(String urlString) {

        BufferedReader reader = null;
        HttpURLConnection urlConnection = null;
        String stream = null;
        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(SET_READ_TIMEOUT);
            urlConnection.setConnectTimeout(SET_CONNECT_TIMEOUT);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            // Check the connection status
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream ins = urlConnection.getInputStream();
                StringBuilder builder = new StringBuilder();
                if (ins == null) {
                    return null;
                }

                reader = new BufferedReader(new InputStreamReader(ins));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                if (builder.length() == 0) {
                    return null;
                }

                stream = builder.toString();
            } else {
                return null;
            }
        } catch (IOException e) {
            Log.e(TAG, "error: " + e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, "error: " + e);
                }
            }
        }

        return stream;
    }
}