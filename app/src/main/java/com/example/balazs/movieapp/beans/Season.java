package com.example.balazs.movieapp.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * This class is a simple bean to store the details of a season
 * Created by Balázs on 16/03/22.
 */
public class Season implements Parcelable {

    private static String title;
    private static String season;

    private String episodeTitle;
    private String episodeReleased;
    private String episode;
    private String episodeImdbRating;
    private String episodeImdbID;

    public Season(String episodeTitle, String episodeReleased, String episode, String episodeImdbRating,
                  String episodeImdbID) {
        this.episodeTitle = episodeTitle;
        this.episodeReleased = episodeReleased;
        this.episode = episode;
        this.episodeImdbRating = episodeImdbRating;
        this.episodeImdbID = episodeImdbID;
    }


    protected Season(Parcel in) {
        episodeTitle = in.readString();
        episodeReleased = in.readString();
        episode = in.readString();
        episodeImdbRating = in.readString();
        episodeImdbID = in.readString();
    }

    public static final Creator<Season> CREATOR = new Creator<Season>() {
        @Override
        public Season createFromParcel(Parcel in) {
            return new Season(in);
        }

        @Override
        public Season[] newArray(int size) {
            return new Season[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(episodeTitle);
        parcel.writeString(episodeReleased);
        parcel.writeString(episode);
        parcel.writeString(episodeImdbRating);
        parcel.writeString(episodeImdbID);
    }

    public static String getTitle() {
        return title;
    }

    public static void setTitle(String title) {
        Season.title = title;
    }

    public static String getSeason() {
        return season;
    }

    public static void setSeason(String season) {
        Season.season = season;
    }

    public String getEpisodeTitle() {
        return episodeTitle;
    }

    public void setEpisodeTitle(String episodeTitle) {
        this.episodeTitle = episodeTitle;
    }

    public String getEpisodeReleased() {
        return episodeReleased;
    }

    public void setEpisodeReleased(String episodeReleased) {
        this.episodeReleased = episodeReleased;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getEpisodeImdbRating() {
        return episodeImdbRating;
    }

    public void setEpisodeImdbRating(String episodeImdbRating) {
        this.episodeImdbRating = episodeImdbRating;
    }

    public String getEpisodeImdbID() {
        return episodeImdbID;
    }

    public void setEpisodeImdbID(String episodeImdbID) {
        this.episodeImdbID = episodeImdbID;
    }


}
