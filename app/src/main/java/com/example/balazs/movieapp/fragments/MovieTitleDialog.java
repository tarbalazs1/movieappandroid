package com.example.balazs.movieapp.fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.autocomplete.SQLiteAdapter;
import com.example.balazs.movieapp.autocomplete.SQLiteTitleObject;
import com.example.balazs.movieapp.beans.Title;
import com.example.balazs.movieapp.interfaces.IDatabaseChange;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;

/**This class is a DialogFragment that shows the first ten results of the search
 * Created by Balázs on 2016.02.14..
 */
public class MovieTitleDialog extends android.support.v4.app.DialogFragment implements
        AdapterView.OnItemClickListener {


    private ListView mListView;
    private ArrayList<String> mMovieTitleList;
    private IDatabaseChange mDatabaseChange;

    /**
     * Lifecycle method creates and returns the view hierarchy associated with the fragment
     *
     * @param inflater LayoutInflater object that can be used to inflate any views
     * @param container The parent view
     * @param savedInstanceState bundle that used for return the saved data
     * @return Return the View for the fragment's UI
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.title_dialog, container, false);
        final Drawable d = new ColorDrawable(Color.BLACK);
        d.setAlpha(130);
        getDialog().getWindow().setBackgroundDrawable(d);
        mListView = (ListView) view.findViewById(R.id.list);
        if (savedInstanceState != null) {
            mMovieTitleList = savedInstanceState.getStringArrayList("MovieTitleList");
        }

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    /**
     * When the activity is created this method is called where I initialize IDatabaseChange and
     * the ArrayAdapter and set the ItemClickListener
     * @param savedInstanceState bundle for savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        //nem volt container az inflateben és akkor elszállt forgatásnál vagy duplán rakta ki
        //getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        mDatabaseChange = (IDatabaseChange) getParentFragment();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                R.layout.row_dialog_list, R.id.lv_item, mMovieTitleList);

        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(this);

    }

    /**
     * Called to retrieve per-instance state from an activity before being killed so that the
     * state can be restored in onCreate(Bundle) or onRestoreInstanceState(Bundle)
     * @param outState Bundle in which to place your saved state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("MovieTitleList", mMovieTitleList);
    }


    /**
     * Callback method to be invoked when an item in this AdapterView has been clicked. In this method
     * I set some animation, and add the chosen title to the realm database and sqlite database
     * and call the onDatabaseChanged() method
     * @param parent The AdapterView where the click happened
     * @param view The view within the AdapterView that was clicked
     * @param position  The position of the view in the adapter
     * @param id The row id of the item that was clicked
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.listview_row_anim);
        view.startAnimation(hyperspaceJumpAnimation);

        long now = System.currentTimeMillis();
        String movieTitle = mMovieTitleList.get(position);

        Realm realm = Realm.getDefaultInstance();
        Title title = new Title(movieTitle, now);
        if (!checkIfExists(realm, title.getMovieTitle())) {
            realm.beginTransaction();
            realm.copyToRealm(title);
            //itt ment a hiba mert nem commitoltam a tranzakciot
            realm.commitTransaction();
            realm.close();
        }
        /*SQLiteAdapter sQLiteAdapter = new SQLiteAdapter(getActivity());
        sQLiteAdapter.create(new SQLiteTitleObject(title.getMovieTitle()));*/
        mDatabaseChange.onDatabaseChanged(title.getMovieTitle());

       /* Realm realm = Realm.getDefaultInstance();
        long now = System.currentTimeMillis();
        Title title = new Title(now, mMovieTitleList.get(position));
        if (!checkIfExists(realm, title.getMovietTitle())) {
            realm.beginTransaction();
            realm.copyToRealm(title);
            //itt ment a hiba mert nem commitoltam a tranzakciot
            realm.commitTransaction();
            realm.close();
        }*/


        dismiss();

    }

    /**Check if the current title already exists in realm database or not
     * @param realm Realm object
     * @param movieTitle current movie title
     * @return return true or false exists or not
     */
    public boolean checkIfExists(Realm realm, String movieTitle) {

        RealmQuery<Title> query = realm.where(Title.class)
                .equalTo("movieTitle", movieTitle);

        return query.count() != 0;
    }

    public void setmMovieTitleList(ArrayList<String> mMovieTitleList) {
        this.mMovieTitleList = mMovieTitleList;
    }

    public void setParentFragment(FragmentMovieChooser parentFragment) {
        this.mDatabaseChange = parentFragment;
    }
}
