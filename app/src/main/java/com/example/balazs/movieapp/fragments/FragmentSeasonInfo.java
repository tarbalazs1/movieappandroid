package com.example.balazs.movieapp.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.balazs.movieapp.classes.AsyncMovie;
import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.activities.ActivityEpisodeInfo;
import com.example.balazs.movieapp.adapters.SeasonListAdapter;
import com.example.balazs.movieapp.beans.Episode;
import com.example.balazs.movieapp.beans.Season;
import com.example.balazs.movieapp.classes.EpisodeSorter;
import com.example.balazs.movieapp.classes.ViewPoint;
import com.example.balazs.movieapp.extras.Util;
import com.example.balazs.movieapp.interfaces.ISeasonImage;

import java.util.Collections;
import java.util.List;

/**This is a fragment class that shows the details of the current season
 * Created by Balázs on 16/03/22.
 */
public class FragmentSeasonInfo extends Fragment implements AdapterView.OnItemClickListener,
        LoaderManager.LoaderCallbacks<Object>, ISeasonImage {

    private final String TAG = this.getClass().getCanonicalName();

    private static final int EPISODE_INFO_TASK_ID = 5;

    private List<Season> mSeasonList;
    private TextView mTvSeasonTitle;
    private SeasonListAdapter mSeasonListAdapter;
    private ListView mSeasonListView;

    private String mUrl;
    private boolean mAsyncBoolean;
    private int mAsyncID;


    /**
     * Lifecycle method creates and returns the view hierarchy associated with the fragment
     *
     * @param inflater LayoutInflater object that can be used to inflate any views
     * @param container The parent view
     * @param savedInstanceState bundle that used for return the saved data
     * @return Return the View for the fragment's UI
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_season_info, container, false);
        setHasOptionsMenu(true);
        mTvSeasonTitle = (TextView) rootView.findViewById(R.id.tv_season_title);
        mSeasonListView = (ListView) rootView.findViewById(R.id.lv_season);

        if (savedInstanceState != null) {
            mAsyncBoolean = savedInstanceState.getBoolean("Async");
            mAsyncID = savedInstanceState.getInt("AsyncID");

        }
        if (mAsyncID != 0) {
            Log.i(TAG, "if else " + mAsyncID);

            getActivity().getSupportLoaderManager().initLoader(mAsyncID, null, this).forceLoad();
        }
        return rootView;
    }

    /**
     * When the activity is created we can get the Season object
     * @param savedInstanceState bundle for savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSeasonList = getActivity().getIntent().getParcelableArrayListExtra("SEASON");
        setSeasonData();
        mSeasonListAdapter = new SeasonListAdapter(getContext(), mSeasonList, this);
        mSeasonListView.setAdapter(mSeasonListAdapter);
        mSeasonListView.setOnItemClickListener(this);
    }

    /**
     * Set the title of the current season with some html tags for better UI/UX
     */
    private void setSeasonData() {
        mTvSeasonTitle.setText(Html.fromHtml(getResources().getString(R.string.html_title_start_tag) + Season.getTitle()
                + getResources().getString(R.string.str_ep_season_letter) + Season.getSeason() +
                getResources().getString(R.string.html_title_end_tag)));
    }

    /**
     * Callback method to be invoked when an item in this Season adapter has been clicked.
     * @param parent The AdapterView where the click happened
     * @param view The view within the AdapterView that was clicked
     * @param position  The position of the view in the adapter
     * @param id The row id of the item that was clicked
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Season season = (Season) mSeasonListView.getItemAtPosition(position);
        mUrl = Util.urlBuilder(Season.getTitle(), Util.EPISODE_DETAILS_TAG, 0, 0, null, Season.getSeason(), season.getEpisode());

        mAsyncID = EPISODE_INFO_TASK_ID;
        if (!mAsyncBoolean) {
            mAsyncBoolean = true;
            getActivity().getSupportLoaderManager().initLoader(EPISODE_INFO_TASK_ID, null, this).forceLoad();

        } else {
            getActivity().getSupportLoaderManager().restartLoader(EPISODE_INFO_TASK_ID, null, this).forceLoad();
        }
    }

    /**Instantiate and return a new Loader for the given ID.
     * @param id The ID whose loader is to be created
     * @param args Any arguments supplied by the caller
     * @return
     */
    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new AsyncMovie(getActivity(), mUrl);
    }

    /**Called when a previously created loader has finished its load.
     * @param loader The Loader that has finished.
     * @param data The data generated by the Loader. It is an Episode object
     */
    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        if (data != null && loader.getId() == EPISODE_INFO_TASK_ID && data instanceof Episode) {
            startEpisodeInfoPage((Episode) data);
        }
        mAsyncID = 0;
    }

    /**Start a new activity with the data of the chosen episode
     * @param data chosen Episode object
     */
    private void startEpisodeInfoPage(Episode data) {
        Intent i = new Intent(getActivity(), ActivityEpisodeInfo.class);
        Log.i("VMI", data.getTitle());
        i.putExtra("EPISODE", data);
        startActivity(i);
    }

    /**Called when a previously created loader is being reset, and thus making its data unavailable.
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    /**
     * Called to retrieve per-instance state from an activity before being killed so that the
     * state can be restored in onCreate(Bundle) or onRestoreInstanceState(Bundle)
     * @param outState Bundle in which to place my saved state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("Async", mAsyncBoolean);
        outState.putInt("AsyncID", mAsyncID);
        super.onSaveInstanceState(outState);
    }

    /**
     *  Create the options menu, inflate the menu xml file
     * @param menu The options menu in which you place your items
     * @param inflater MenuInflater object
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sort, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * This method is called when the user select an item from options menu
     * @param item The selected item
     * @return Super call
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.sortByTitle: {
                EpisodeSorter.setChosenViewPoint(ViewPoint.TITLE, EpisodeSorter.INC);
                Collections.sort(mSeasonList, new EpisodeSorter());
                mSeasonListAdapter.notifyDataSetChanged();
            }
            break;
            case R.id.sortByRating: {
                EpisodeSorter.setChosenViewPoint(ViewPoint.RATING, EpisodeSorter.INC);
                Collections.sort(mSeasonList, new EpisodeSorter());
                mSeasonListAdapter.notifyDataSetChanged();
            }
            break;
            case R.id.sortByDate: {
                EpisodeSorter.setChosenViewPoint(ViewPoint.DATE, EpisodeSorter.INC);
                Collections.sort(mSeasonList, new EpisodeSorter());
                mSeasonListAdapter.notifyDataSetChanged();
            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * When the user clicked on imdb icon than the app shows the imdb page of the selected episode
     * @param position position of the selected episode in the adapter
     */
    @Override
    public void onEpisodeImdbClick(int position) {
        Season season = mSeasonListAdapter.getItem(position);
        if (position != ListView.INVALID_POSITION) {
            Util.startImdbWebPage(season.getEpisodeImdbID(), getContext());
        }
    }
}
