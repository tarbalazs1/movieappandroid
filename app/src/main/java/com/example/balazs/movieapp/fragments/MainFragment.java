package com.example.balazs.movieapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.balazs.movieapp.R;
import com.example.balazs.movieapp.extras.Util;
import com.example.balazs.movieapp.interfaces.ICommunicator;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

/**This is a fragment class that shows the movie and cinema buttons, and handle with Facebook login
 * Created by Balázs on 16/04/14.
 */
public class MainFragment extends Fragment implements View.OnClickListener {

    private TextView mTextDetails;
    private CallbackManager mCallbackManager;
    private AccessTokenTracker mTokenTracker;
    private ProfileTracker mProfileTracker;
    private Button btnMap;
    private Button btnMovies;
    private ICommunicator communicator;
    private final String TAG = MainFragment.class.getCanonicalName();

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        /**
         * Called when the dialog completes without error.
         * @param loginResult Result from the dialog
         */
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d(TAG, "onSuccess");
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            mTextDetails.setText(constructWelcomeMessage(profile));

        }


        /**
         * Called when the dialog is canceled.
         */
        @Override
        public void onCancel() {
            Log.d(TAG, "onCancel");
        }

        /**Called when the dialog finishes with an error.
         * @param e The error that occurred
         */
        @Override
        public void onError(FacebookException e) {
            Log.d(TAG, "onError " + e);
        }
    };

    /**
     * Called when the activity is created, initialize some variable here
     * @param savedInstanceState bundle for savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnMap = (Button) getActivity().findViewById(R.id.btncinemas);
        btnMovies = (Button) getActivity().findViewById(R.id.btnmovies);
        communicator = (ICommunicator) getActivity();
        btnMovies.setOnClickListener(this);
        btnMap.setOnClickListener(this);
    }


    /**
     * Called to do initial creation of a fragment
     * @param savedInstanceState If the fragment is being re-created from a previous saved state, this is the state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCallbackManager = CallbackManager.Factory.create();
        setupTokenTracker();
        setupProfileTracker();

        mTokenTracker.startTracking();
        mProfileTracker.startTracking();
    }


    /**
     * Called to have the fragment instantiate its user interface view
     *
     * @param inflater LayoutInflater object that can be used to inflate any views in the fragment
     * @param container this is the parent view that the fragment's UI should be attached to
     * @param savedInstanceState  this fragment is being re-constructed from a previous saved state as given here
     * @return Return the View for the fragment's UI, or null
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    /**
     * Called immediately after onCreateView(LayoutInflater, ViewGroup, Bundle) has returned
     * In this method the setupTextDetails(view) and setupLoginButton(view) methods are called
     * @param view The View returned by onCreateView(LayoutInflater, ViewGroup, Bundle)
     * @param savedInstanceState this fragment is being re-constructed from a previous saved state as given here
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setupTextDetails(view);
        setupLoginButton(view);
    }

    /**
     * Lifecycle method which called when the fragment is visible to the user and actively running
     */
    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        mTextDetails.setText(constructWelcomeMessage(profile));
    }

    /**
     * Lifecycle method which called when the Fragment is no longer started
     */
    @Override
    public void onStop() {
        super.onStop();
        mTokenTracker.stopTracking();
        mProfileTracker.stopTracking();
    }

    /**
     * Called when an activity you launched exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     * @param requestCode The integer request code originally supplied to startActivityForResult(),
     *                    allowing you to identify who this result came from
     * @param resultCode The integer result code returned by the child activity through its setResult()
     * @param data An Intent, which can return result data to the caller
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**TextView initialization
     * @param view View object
     */
    private void setupTextDetails(View view) {
        mTextDetails = (TextView) view.findViewById(R.id.tvWelcome);
    }

    /**
     * AccessTokenTracker Initialization with its onCurrentAccessTokenChanged(AccessToken
     * oldAccessToken, AccessToken currentAccessToken) method
     */
    private void setupTokenTracker() {
        mTokenTracker = new AccessTokenTracker() {
            /**
             *The method that will be called with the access token changes
             * @param oldAccessToken The access token before the change
             * @param currentAccessToken 	The new access token
             */
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.d(TAG, "" + currentAccessToken);

            }
        };
    }

    /**
     * ProfileTracker initialization with its onCurrentProfileChanged(Profile oldProfile, Profile
     * currentProfile) method
     */
    private void setupProfileTracker() {
        mProfileTracker = new ProfileTracker() {
            /**
             * The method that will be called when the profile changes.
             * @param oldProfile The profile before the change
             * @param currentProfile The new profile
             */
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                Log.d(TAG, "" + currentProfile);
                mTextDetails.setText(constructWelcomeMessage(currentProfile));
            }
        };
    }

    /**
     * This method sets up the Facebook login button
     * @param view View object
     */
    private void setupLoginButton(View view) {
        LoginButton mButtonLogin = (LoginButton) view.findViewById(R.id.login_button);
        mButtonLogin.setFragment(this);
        mButtonLogin.setReadPermissions(getString(R.string.str_faecbook_permission));
        mButtonLogin.registerCallback(mCallbackManager, mFacebookCallback);
    }

    /**This method sets the welcome message according to the current user's name if the profile is not null
     * @param profile Facebook profile object
     * @return return with the welcome message
     */
    private String constructWelcomeMessage(Profile profile) {
        StringBuffer stringBuffer = new StringBuffer();
        if (profile != null) {
            stringBuffer.append(getResources().getString(R.string.str_facebook_welcome) + profile.getName());
        }
        return stringBuffer.toString();
    }


    /**
     * onClick method for the buttons that called when a view has been clicked
     * @param v the view that was clicked
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btncinemas:
                if (Util.isNetworkAvailable(getActivity())) {
                    communicator.onModeSelected(Util.STRING_CASE_MAPS_ACTIVITY);
                }else{
                    Toast.makeText(getActivity(), R.string.str_toast_no_internet, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnmovies:
                if (Util.isNetworkAvailable(getActivity())) {
                    communicator.onModeSelected(Util.STRING_CASE_MOVIE_ACTIVITY);
                }else{
                    Toast.makeText(getActivity(), R.string.str_toast_no_internet, Toast.LENGTH_SHORT).show();
                }                break;
        }

    }
}
