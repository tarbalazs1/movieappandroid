package com.example.balazs.movieapp.beans;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * This class is a simple bean to store the details of an movie
 * Created by Balázs on 16/03/15.
 */
public class Movie implements Parcelable {

    private String title;
    private String year;
    private String rated;
    private String released;
    private String runtime;
    private String genre;
    private String director;
    private String writer;
    private String actors;
    private String plot;
    private String language;
    private String country;
    private String awards;
    private String posterUrl;
    private String metascore;
    private String imdbRating;
    private String imdbVotes;
    private String type;
    private String imdbID;
    private String totalSeasons;
    private boolean response;
    private static Bitmap poster;


    public Movie(String title, String released, String runtime, String plot, String language,
                 String country, String posterUrl, String imdbRating, String type, String imdbID,
                 String totalSeasons) {
        this.title = title;
        this.released = released;
        this.runtime = runtime;
        this.plot = plot;
        this.language = language;
        this.country = country;
        this.posterUrl = posterUrl;
        this.imdbRating = imdbRating;
        this.type = type;
        this.imdbID = imdbID;
        this.totalSeasons = totalSeasons;
    }



    protected Movie(Parcel in) {
        title = in.readString();
        year = in.readString();
        rated = in.readString();
        released = in.readString();
        runtime = in.readString();
        genre = in.readString();
        director = in.readString();
        writer = in.readString();
        actors = in.readString();
        plot = in.readString();
        language = in.readString();
        country = in.readString();
        awards = in.readString();
        posterUrl = in.readString();
        metascore = in.readString();
        imdbRating = in.readString();
        imdbVotes = in.readString();
        type = in.readString();
        imdbID = in.readString();
        totalSeasons = in.readString();
        response = in.readByte() != 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(year);
        parcel.writeString(rated);
        parcel.writeString(released);
        parcel.writeString(runtime);
        parcel.writeString(genre);
        parcel.writeString(director);
        parcel.writeString(writer);
        parcel.writeString(actors);
        parcel.writeString(plot);
        parcel.writeString(language);
        parcel.writeString(country);
        parcel.writeString(awards);
        parcel.writeString(posterUrl);
        parcel.writeString(metascore);
        parcel.writeString(imdbRating);
        parcel.writeString(imdbVotes);
        parcel.writeString(type);
        parcel.writeString(imdbID);
        parcel.writeString(totalSeasons);
        parcel.writeByte((byte) (response ? 1 : 0));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getMetascore() {
        return metascore;
    }

    public void setMetascore(String metascore) {
        this.metascore = metascore;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getImdbVotes() {
        return imdbVotes;
    }

    public void setImdbVotes(String imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getTotalSeasons() {
        return totalSeasons;
    }

    public void setTotalSeasons(String totalSeasons) {
        this.totalSeasons = totalSeasons;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public static Bitmap getPoster() {
        return poster;
    }

    public static void setPoster(Bitmap poster) {
        Movie.poster = poster;
    }
}