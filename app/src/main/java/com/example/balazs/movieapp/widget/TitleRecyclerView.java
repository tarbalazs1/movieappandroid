package com.example.balazs.movieapp.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.example.balazs.movieapp.extras.Util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/** This class is an observer class for watching changes
 * Created by Balázs on 16/03/04.
 */
public class TitleRecyclerView extends RecyclerView {

    private List<View> mEmptyViews = Collections.emptyList();
    private List<View> mEmptyTextViews = Collections.emptyList();

    /**
     * Observer base class for watching changes to an RecyclerView.Adapter
     */
    private AdapterDataObserver mObserver = new AdapterDataObserver() {

        /**
         * Called when there was a change
         */
        @Override
        public void onChanged() {
            toggleViews();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            toggleViews();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            toggleViews();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            toggleViews();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            toggleViews();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            toggleViews();
        }
    };

    /**
     * This method is called when there was some changes. According to the count of items this method
     * show, or hide some views
     */
    private void toggleViews() {

        if (getAdapter() != null && !mEmptyViews.isEmpty() && !mEmptyTextViews.isEmpty()) {
            if (getAdapter().getItemCount() == 0) {

                Util.showViews(mEmptyViews);


                setVisibility(View.GONE);

                Util.hideViews(mEmptyTextViews);
            } else {

                Util.hideViews(mEmptyViews);

                setVisibility(View.VISIBLE);

                Util.showViews(mEmptyTextViews);
            }
        }
    }

    /**Set a new adapter to provide child views on demand
     * @param adapter The new adapter to set
     */
    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mObserver);
        }
        mObserver.onChanged();
    }

    public TitleRecyclerView(Context context) {
        super(context);
    }

    public TitleRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TitleRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void showIfEmpty(View ...emptyViews) {
        mEmptyViews = Arrays.asList(emptyViews);
    }

    public void hideIfEmpty(View ...mMovieList) {
        mEmptyTextViews = Arrays.asList(mMovieList);
    }
}
