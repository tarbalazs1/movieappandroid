package com.example.balazs.movieapp;

import android.test.InstrumentationTestCase;

import com.example.balazs.movieapp.interfaces.SwipeListener;

/**
 * Created by Balázs on 16/03/29.
 */
public class SwipeListenerTest extends InstrumentationTestCase implements SwipeListener {

    private static final int POS = 10;
    SwipeListenerTest swipeListenerTest;
    SwipeListener swipeListener;

    public void testSwipeListener() throws Exception {
        swipeListenerTest = new SwipeListenerTest();
        swipeListener = swipeListenerTest;
        swipeListener.onSwipe(POS);

    }

    @Override
    public void onSwipe(int position) {
        assertEquals(POS, position);
    }
}
