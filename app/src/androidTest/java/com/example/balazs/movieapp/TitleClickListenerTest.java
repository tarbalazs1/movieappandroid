package com.example.balazs.movieapp;

import android.test.InstrumentationTestCase;

import com.example.balazs.movieapp.interfaces.TitleClickListener;

/**
 * Created by Balázs on 16/03/29.
 */
public class TitleClickListenerTest extends InstrumentationTestCase implements TitleClickListener {

    TitleClickListenerTest titleClickListenerTest;
    TitleClickListener titleClickListener;
    private final int POS = 12;

    public void testTitleClickListener() throws Exception {
        titleClickListenerTest = new TitleClickListenerTest();
        titleClickListener = titleClickListenerTest;
        titleClickListener.onTitleClick(POS);

    }

    @Override
    public void onTitleClick(int position) {
        assertEquals(position, POS);

    }
}
