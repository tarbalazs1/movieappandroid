package com.example.balazs.movieapp;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.test.ActivityInstrumentationTestCase2;
import android.test.InstrumentationTestCase;

import com.example.balazs.movieapp.activities.ActivityMovieChooser;
import com.example.balazs.movieapp.beans.Movie;
import com.example.balazs.movieapp.classes.AsyncMovie;

/**
 * Created by Balázs on 16/04/24.
 */
public class XMLResponseTest extends ActivityInstrumentationTestCase2<ActivityMovieChooser>
        implements LoaderManager.LoaderCallbacks<Object> {

    private final int CHOSEN_TITLE_TASK_ID = 2;
    private final String URL = "http://www.omdbapi.com/?t=supernatural&y=&plot=full&r=json";
    private String expectedTitle = "Supernatural";
    private boolean mAsyncBoolean;


    public XMLResponseTest() {
        super(ActivityMovieChooser.class);
    }

    public void testWorkerThread() {
        if (!mAsyncBoolean) {
            mAsyncBoolean = true;
            getActivity().getSupportLoaderManager().initLoader(CHOSEN_TITLE_TASK_ID, null, this).forceLoad();
        } else {
            getActivity().getSupportLoaderManager().restartLoader(CHOSEN_TITLE_TASK_ID, null, this).forceLoad();
        }
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new AsyncMovie(getActivity(), URL);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        assertTrue(data != null);
        assertTrue(((Movie)data).getTitle().equals(expectedTitle));
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }
}
