package com.example.balazs.movieapp;

import android.test.ActivityInstrumentationTestCase2;

import com.example.balazs.movieapp.activities.ActivityMovieChooser;
import com.example.balazs.movieapp.autocomplete.SQLiteAdapter;
import com.example.balazs.movieapp.autocomplete.SQLiteTitleObject;

/**
 * Created by Balázs on 16/03/29.
 */
public class DatabaseTest extends ActivityInstrumentationTestCase2<ActivityMovieChooser> {

    private SQLiteAdapter sqLiteAdapter;
    private String movieTitle = "Game of Thrones";
    private String wrongMovieTitle = "Bob's Burger";
    private String[] content;
    private final int expectedValue = 1;
    private final int expectedValueIndex = 0;
    private final int expectedValueAfterWrongTitle = 2;


    public DatabaseTest() {
        super(ActivityMovieChooser.class);
    }


    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void testDatabaseInsert() {
        sqLiteAdapter = new SQLiteAdapter(getActivity());
        sqLiteAdapter.create(new SQLiteTitleObject(movieTitle));
        content = sqLiteAdapter.read();
        assertEquals(movieTitle, content[expectedValueIndex]);
        assertNotSame(wrongMovieTitle, content[expectedValueIndex]);
        assertEquals(expectedValue, content.length);
        sqLiteAdapter.close();

    }

    public void testDatabaseSameTitle() {
        sqLiteAdapter = new SQLiteAdapter(getActivity());
        sqLiteAdapter.create(new SQLiteTitleObject(movieTitle));
        content = sqLiteAdapter.read();
        assertEquals(expectedValue, content.length);
        sqLiteAdapter.close();
    }

    public void testDatabaseWithSpecialChar() {
        sqLiteAdapter = new SQLiteAdapter(getActivity());
        sqLiteAdapter.create(new SQLiteTitleObject(wrongMovieTitle));
        content=sqLiteAdapter.read();
        assertEquals(expectedValueAfterWrongTitle, content.length);
        sqLiteAdapter.close();
    }

}
