package com.example.balazs.movieapp;

import android.test.InstrumentationTestCase;

import com.example.balazs.movieapp.interfaces.ICommunicator;

/**
 * Created by Balázs on 16/03/29.
 */
public class ICommunicatorTest extends InstrumentationTestCase implements ICommunicator {

    private static final String TEST_TAG = "TEST_TAG";
    ICommunicatorTest iCommunicatorTest;
    ICommunicator iCommunicator;

    public void testICommunicator() throws Exception {
        iCommunicatorTest = new ICommunicatorTest();
        iCommunicator = iCommunicatorTest;
        iCommunicator.onModeSelected(TEST_TAG);

    }

    @Override
    public void onModeSelected(String tag) {
        assertEquals(TEST_TAG, tag);
    }
}
