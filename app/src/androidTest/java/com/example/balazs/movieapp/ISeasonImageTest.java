package com.example.balazs.movieapp;

import android.test.InstrumentationTestCase;

import com.example.balazs.movieapp.interfaces.ISeasonImage;

/**
 * Created by Balázs on 16/04/24.
 */
public class ISeasonImageTest extends InstrumentationTestCase implements ISeasonImage {

    private final int POS = 10;
    ISeasonImage iSeasonImage;
    ISeasonImageTest iSeasonImageTest;

    public void testISeasonImage() {
        iSeasonImageTest = new ISeasonImageTest();
        iSeasonImage = iSeasonImageTest;
        iSeasonImage.onEpisodeImdbClick(POS);
    }

    @Override
    public void onEpisodeImdbClick(int position) {
        assertEquals(POS, position);
    }
}
